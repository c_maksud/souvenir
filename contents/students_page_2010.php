<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-pills">
            <li class="active"><a data-target="#2010_contests" data-toggle="tab">Software Competition</a></li>
            <li><a data-target="#2010_acm" data-toggle="tab">Programming Comtests</a></li>
            <li><a data-target="#2010_excellence" data-toggle="tab">Excellence</a></li>
        </ul>
        <div class="tab-content">
            <div id="2010_contests" class="tab-pane active">
                <?php include_once 'students_page_2010_contests.php';?>
            </div>
            <div id="2010_acm" class="tab-pane">
                <?php include_once 'students_page_2010_acm.php';?>
            </div>
            <div id="2010_excellence" class="tab-pane">
                <?php include_once 'students_page_2010_excellence.php';?>
            </div>
        </div>
    </div>
</div>