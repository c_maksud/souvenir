<div class="container-fluid">
    <div class="row-fluid">
        <table class="table">
            <tr id="06">
            <h1 align="center"><span style="font-size: large;"><strong><span style="text-decoration: underline;">Software Project of BUET- CSE- Batch &rsquo;06</span></strong></span></h1>
            <div>
                <p><strong>&nbsp;</strong></p>
                <p><span style="font-size: medium;"><strong>Project Name: </strong><strong>Digital Waqt-al-salat</strong>(Champion, Inter-BUET CSE Project Contest 2011, category Level-4, Term-l)</span></p>
            </div>
            <p><span style="font-size: medium;"><strong>Team Members: </strong></span></p>
            <ul>
                <li><span style="font-size: medium;">Md. Badsha Molla(0605080)</span></li>
                <li><span style="font-size: medium;">Md. Abu Hossain Russel Mahmud (0605081)</span></li>
                <li><span style="font-size: medium;">Rakibul Hasan (0605062)</span></li>
            </ul>
            <p><span style="font-size: medium;"><strong>Project Details:</strong></span></p>
            <p><span style="font-size: medium;">This project helps the muslim to determine the exact time of salat around the whole year. This is mobile app and platform is android. This degitalization of salat timing is a demanded app. And This project is quite user friendly and a well thought project.</span></p>
            <p align="center"><span style="font-size: medium;"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></span></p>
            <p align="center"><span style="font-size: medium;"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></span></p>
            <p align="center"><span style="font-size: medium;"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></span></p>
            <div>
                <p><span style="font-size: medium;"><strong>Project Name: </strong><strong>Health monitor</strong> (Runner Up, Inter-BUET CSE Project Contest 2011, category Level-4, Term-l)</span></p>
            </div>
            <p><span style="font-size: medium;"><strong>Team Members: </strong></span></p>
            <ul>
                <li><span style="font-size: medium;">Saima Sultana Tithi (0605005)</span></li>
                <li><span style="font-size: medium;">Sarah Harun (0605039)</span></li>
            </ul>
            <p><span style="font-size: medium;"><strong>Project Details:</strong></span></p>
            <p><span style="font-size: medium;">This android based project determines the health contion of the mobile user. Using the various sensors like, temperatue, proximity amd others, this app will&nbsp; determine the temperature, heart beat, blood pressure, skin tone and others. Then with these information few diease can be auto detected or a doctor or a consultant can suggest better advice to patients. It is really an excellent app for the deprived and remote area and in emergency situation where immediate can&rsquo;t be arrived, like earthquake, flood.</span></p>
            </tr>
            <tr id="07">
            <h1 align="center"><span style="font-size: large;"><strong><span style="text-decoration: underline;">Software Project of BUET- CSE- Batch &rsquo;07</span></strong></span></h1>
            <div>
                <p><strong>&nbsp;</strong></p>
                <p><span style="font-size: medium;"><strong>Project Name: </strong><strong>CSE Office Management</strong> (Champion, Inter-BUET CSE Project Contest 2011, category Level-3, Term-ll)</span></p>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <img  src="assets/images/software/image001.jpg"/>
                </div>
            </div>
            <p><span style="font-size: medium;"><strong>Team Members: </strong></span></p>
            <ul>
                <li><span style="font-size: medium;">Eshita Zaman (0705065)</span></li>
                <li><span style="font-size: medium;">Fahim Tahmid Chowdhury (0705074)</span></li>
            </ul>
            <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
            <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
            <p><span style="font-size: medium;"><strong>Project Details:</strong></span></p>
            <p><span style="font-size: medium;">This project is the complete office management solution for an academic system, and especially designed for CSE Office. Different interactions among various parts of the administration and management section are managed and designed fluently and easily to understand. Information is transparent and in the same time secure. This is a perfect example of e-governance.</span></p>
            <p><span style="font-size: medium;"><strong>Project </strong><strong>Name: </strong><strong>Online Courier Service</strong> (Runner Up, Inter-BUET CSE Project Contest 2011, category Level-3, Term-ll)</span></p>
            <div class="row-fluid">
                <div class="span6">
                    <img  src="assets/images/software/image003.jpg"/>
                </div>
            </div>
            <p><span style="font-size: medium;"><strong>Team Members: </strong></span></p>
            <ul>
                <li><span style="font-size: medium;">Dipankar Ranjan Baisya (0705018)</span></li>
                <li><span style="font-size: medium;">Mir Md. Faysal (0705026)</span></li>
            </ul>
            <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
            <p><span style="font-size: medium;"><strong>Project Details:</strong></span></p>
            <p><span style="font-size: medium;">Still in the Digital Bangladesh, Online Shopping is not very popular. Because of its lack of transparency and good service, it is not popular yet. &lsquo;Online Courier Service&rsquo; is the solution of this situation. This app makes the online shopping easy and transparent.And home delivery system make an attractive addition to it.&lsquo;Money on delivery&rsquo; is the motto of this system.</span></p>
            </tr>
            <tr id="08">
            <h1 align="center"><span style="font-size: large;"><strong><span style="text-decoration: underline;">Software Project of BUET- CSE- Batch &rsquo;08</span></strong></span></h1>
            <table class="table table-bordered">
                <tbody>
                    <tr id="samsung-contest">
                <div>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <h1><span style="font-size: medium;"><strong>Project Name: </strong>Tell Tale &ndash; Approach to communal writing</span></h1>
                    <h1><span style="font-size: large;">Samsung Android App Contest (Gold Status Winner):</span></h1>
                    <div class="row-fluid">
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/students/image040.jpg"/>
                        </div>
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/students/image042.jpg"/>
                        </div>
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/students/image044.jpg"/>
                        </div>
                    </div>
                </div>                
                <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
                <ul>
                    <li><span style="font-size: medium;">Azad Abdus Salam(0805030)</span></li>
                    <li><span style="font-size: medium;">KamrulHasan(0805018)</span></li>
                    <li><span style="font-size: medium;">Tanveer Mohammad Khan(0805027)<br /></span><span style="font-size: medium;"><strong><span style="text-decoration: underline;"> <br /></span></strong></span></li>
                </ul>
                <p><span style="font-size: medium;">Tell Tale &ndash; Approach to communal writing&rdquo; innovates a way to write story/poem/rhyme communally. Each story is assembled by parts, where each part is contributed by different person from the community. This app can have great impact on society by increasing the practice of creative thinking, imagination and open ended writing among people especially the young generation.&nbsp; The Android version of &ldquo;Tell Tale&rdquo; participated in Android Application Development Contest by Samsung Bangladesh R&amp;D Center and received the &ldquo;Gold Status&rdquo; (1st prize) .</span></p>
                </tr>
                <tr id="e-learning-for-autistic">
                <div>
                    <h1><span style="font-size: large;"><strong>Project name:</strong> "E-Learning System and Software for Autistic Children"</span></h1>
                    <h1><span style="font-size: large;">East West University IT Fair 2011(1<sup>st</sup> Prize in Project Section):</span></h1>
                    <img class="img-polaroid" src="assets/images/students/image016.jpg"/>
                </div>                
                <h1 style="font-size: medium;">Team Members:</h1>
                <ul>
                    <li style="font-size: medium;">Anika Anwar (0805083),</li>
                    <li style="font-size: medium;">Ishrat Ahmed Maureen (0805064),</li>
                    <li style="font-size: medium;">SamihaSamrose (0805066),</li>
                    <li style="font-size: medium;">Anan Naha,</li>
                    <li style="font-size: medium;">Syed Ishtiaque Ahmed.</li>
                </ul>
                <p align="center">&nbsp;</p>
                <p  style="font-size: medium;">On 5th February,2011, Anika Anwar,Ishrat Ahmed and SamihaSamrose participated in the inter university software project show arranged by East West University. &ldquo;E-Learning System and Software for Autistic Children&rdquo; was developed for autistic children to help them learn computers and become more interactive socially and mentally. Syed Ishtiaque Ahmed, mentor of the team and Anan Naha from &rsquo;05 batch helped to a great extent. The project won 1<sup>st</sup> prize because of its innovativeness.</p>
                </tr>
                <tr id="hephe">
                <div>
                    <h1><span style="font-size: large;">Project HEPHE(4<sup>th</sup> in Imagine Cup, Bangladesh, 2011)</span></h1>
                    <img class="img-polaroid" src="assets/images/students/image001.jpg"/>
                </div>
                <p align="center">&nbsp;</p>
                <h1 style="font-size: medium;">Team Members:</h1>
                <ul>
                    <li><span style="font-size: medium;">Md. Abdul MunimDibosh (0805033)</span></li>
                    <li><span style="font-size: medium;">Md. MaksudAlamChowdhury (0805034)</span></li>
                    <li><span style="font-size: medium;">Md. NazmulHossain (0805032)</span></li>
                </ul>
                <p><span style="font-size: medium;">&nbsp;</span></p>
                <p><span style="font-size: medium;">The word HEPHE stands for Hunger, Education, Poverty, Health &amp; Environment; a project that was targeted to solve all these severe problems existing in 3<sup>rd</sup> world countries. The primary initiative of the software is to provide primary education in the mean of games and easy to adopt techniques. This creates a bridge in between educated and non-educated people and helps to develop leadership among them. The idea and the developed prototype were appreciated a lot by the then judges of Imagine Cup country round. The software was built using .NET framework, specifically Winforms&amp; Rad Controls for application front end.</span></p>
                </tr>
                <tr id="anvillab">
                <div>
                    <h1><span style="font-size: large;">ANVIL LAB Business Manager</span></h1>
                    <img class="img-polaroid" src="assets/images/students/image020.png"/>
                </div>
                <h1><span style="font-size: medium;">Team members:</span></h1>
                <ul>
                    <li><span style="font-size: medium;">Md. Hasan Al Maruf (0805011)</span></li>
                    <li><span style="font-size: medium;">Sharif Md. SaadGalib (0805014)</span></li>
                    <li><span style="font-size: medium;">Md. Abdul MunimDibosh (0805033)</span></li>
                    <li><span style="font-size: medium;">IrtezaNasir (0805054)</span><span style="font-size: medium;"> <br /></span></li>
                </ul>
                <p><span style="font-size: medium;">&nbsp;</span></p>
                <p><span style="font-size: medium;">In the early summer of 2011, four guys from CSE department were planning to build a sales and marketing chain management software as a project for Software Development course. This software was nothing professional when they started to develop it but it became once Ispahani Group of Bangladesh chose it to implement it for their company. Now this software is being used in more than 6 regional offices of Ispahani by 3000+ users. Developed in latest technologies like Microsoft Silverlight and Windows Azure it has been the first this type of software in Bangladesh; which is developed by only 4 undergrad level students.</span></p>
                </tr>
                <tr id="javfest-pinfriend">
                <div>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: large;"><strong>Project Name:</strong>PinFriend --Identity &amp; Location based Reminder<br />&nbsp;</span></p>
                    <h1><span style="font-size: large;">Therap JAVA Fest (3<sup>rd</sup> Position)</span></h1>
                    <div class="row-fluid">
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/students/image014.jpg"/>
                        </div>
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/students/image012.jpg"/>
                        </div>
                    </div>
                </div>
                <h1><span style="font-size: medium;"><strong>Team Name:</strong> BUET BAREBONES</span></h1>
                <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
                <ul>
                    <li><span style="font-size: medium;">Md. MaksudAlamChowdhury (0805034)</span></li>
                    <li><span style="font-size: medium;">ATM Saleh (0805025)</span></li>
                </ul>                
                <p><span style="font-size: medium;">Therap Java Fest was organized by Therap Services for university students all over the country. Interested participants formed a group of two members and submitted a project proposal and then implemented the idea as a Java web application or an Android-based project. After the Therap Java Fest team evaluated the projects, top ranked participants were invited for a live demonstration of their projects on November 14, 2012. BUET BAREBONESbecame 3rd in the contest and won two Kindles. They developed an Android app named &ldquo;PinFriend --Identity &amp; Location based Reminder&rdquo;</span></p>
                </tr>
                </tbody>
                <tr id="prothom-alo-youth-fest">
                <div>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <h1><span style="font-size:large;"><strong>Project Name:</strong>Location Based Information System Using OpenStreetMap</span></h1>
                    <h1><span style="font-size: large;">TIB-ProthomAlo Youth Festival, Inter University Project Show 2011Champion in Software Category (CUET):</span></h1>
                    <div class="row-fluid">
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/students/image022.jpg"/>
                        </div>
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/students/image024.jpg"/>
                        </div>
                    </div>
                </div>                
                <p><span style="font-size: medium;"><strong>Group Name:</strong> Skyward</span></p>
                <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
                <ul>
                    <li><span style="font-size: medium;">Md.RashidujjamanRifat (0805107)</span></li>
                    <li><span style="font-size: medium;">ShubramiMoutushy (0805082)</span></li>
                </ul>
                <p><span style="font-size: medium;">&nbsp;</span></p>
                <p><span style="font-size: medium;">12-14 May,2011 &ldquo;TIB ProthomAloTarunnoUtsob&rdquo; was held at Chittagong University of Engineering and Technology (CUET). About 16 University participated on that event. The event had Project show, Inter University Debate Competition, Cartoon Exhibition against Corruption and Short film Exhibition.Project show was divided into two categories &ndash; software and Hardware. About 22 projects from 8 different Universities competed there.The judge panel was conducted by TousifSayed (Head of Computer Science and Engineering, Primier University), Professor Dr. DelwarHossain, A.H.M SahanulAlam (Chief Engineer of Intel Soft Solution). In software Category, &ldquo;Location Based Information System Using OpenStreetMap&rdquo; was declared champion.</span></p>
                </tr>
                <tr id="app-a-thon">
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Smart Health Monitor</strong></span></h1>
                    <h1><span style="font-size: large;">Microsoft App-a-thon 2012:Prize in &lsquo;Top Windows Phone App&rsquo;</span></h1>
                    <div class="row-fluid">
                        <div class="span6"><img class="img-polaroid" src="assets/images/students/image026.jpg"/></div>
                    </div>
                </div>
                <h1><span style="font-size: medium;"><strong>Name: &nbsp;</strong>HafsaZannat (0805113)</span></h1>
                <p><span style="font-size: medium;">Microsoft introduces the very first app development marathon, &ldquo;<strong>Microsoft App-a-thon</strong>&rdquo;, in Bangladesh on the Windows 8 and Windows Phone platform. This daylong event took place on the November 3, 2012 in the Institute of Information Technology (IIT) of Dhaka University. Ultimately, the participants come up with 100 apps surpassing the initial target of submitting 80 apps to windows store. Eight of those got the <strong>&lsquo;Top App&rsquo;</strong> award. Students from BUET, DU, AIUB, NSU, KUET, EWU, BRAC University and UIU participated in this event. Besides, as a part of the countrywide preparation, Microsoft Student Partners hosted workshops on those 8 universities.</span></p>
                </tr>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Automated Traffic Alert</strong> (Champion, Inter-BUET CSE Project Contest 2011, category Level-2, Term-ll)</span></h1>
                </div>
                <p><strong><span style="font-size: medium;">Team Members:</span> </strong></p>
                <ul>
                    <li>
                        <p><span style="font-size: medium;">Md. Tanjim Hossain (0805055)</span></p>
                    </li>
                    <li>
                        <p><span style="font-size: medium;">Md. Rabbi Alam (0805029)</span></p>
                    </li>
                    <li>
                        <p><span style="font-size: medium;">A.T.M. Saleh (0805025)</span></p>
                    </li>
                    <li>
                        <p><span style="font-size: medium;">Habibullah Araphat (0805106)</span></p>
                    </li>
                </ul>
                <p><span style="font-size: medium;"><strong>Project Details:</strong></span></p>
                <p><span style="font-size: medium;">In this traffic crowded Dhaka City,&nbsp; &lsquo;Automated Traffic Alert&rsquo; is the perfect solution. This app uses the satellite image of Google map to determine the&nbsp; density of traffic on a certain route. When any user makes a route using the roads, the density of traffic is calculated and choose the for appropriate route for the destination and alerts the user about the possible traffic jam on the streets.</span></p>
                <p align="center"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>
                <p align="center"><strong><span style="text-decoration: underline;">&nbsp;</span></strong></p>                
                </tr>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>SMS based Blood Donation System</strong>(Runner Up, Inter-BUET CSE Project Contest 2011, category Level-2, Term-ll)</span></h1>
                </div>
                <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
                <ul style="font-size: medium;">
                    <li>
                        <p>Shabnam Basera Rishta(0805043)</p>
                    </li>
                    <li>
                        <p>Md. Mahfuzer Rahman(0805057)</p>
                    </li>
                </ul>
                <p style="font-size: medium;"><strong>Project Details:</strong></p>
                <p style="font-size: medium;">Humanity is addressed in the project &lsquo;SMS based Blood Donation System&rsquo;. In the hour of urgent blood need, this app is a very effective and useful. A database of interested blood donor is kept and when the urgency arises, the matched blood donors get a message of possible blood need. Thus it can save many life. As the platform is J2ME, even the cheapest mobile can use this app. This kind of generalization makes the app quite a catch.</p>
                <p>&nbsp;</p>
                </tr>
            </table>
            </tr>
            <tr id="09">
            <h1 align="center"><span style="font-size: large;"><strong><span style="text-decoration: underline;">Software Project of BUET- CSE- Batch &rsquo;09</span></strong></span></h1>
            <table>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Easy Banking Service via Smart App</strong>(4th CITI Finance IT Case Competition &ndash;1<sup>st</sup> Runner Up)</span></h1>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <img class="img-polaroid" src="assets/images/software/image025.jpg"/>
                    </div>
                    <div class="span4">
                        <img class="img-polaroid" src="assets/images/software/image027.png"/>
                    </div>
                    <div class="span4"></div>
                </div>
                <h1><br /><span style="font-size: medium;"> <strong>Team Name:</strong> BUET BRB</span></h1>
                <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
                <ul>
                    <li><span style="font-size: medium;">Taslim Arefin Khan (0905045)</span></li>
                    <li><span style="font-size: medium;">Muhammad Hossain Mahdi (0905008)</span></li>
                    <li><span style="font-size: medium;">Mehrab Bin Morshed (0905022)</span></li>
                    <li><span style="font-size: medium;">Sadia Nahreen (0905006)</span></li>
                </ul>
                <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
                <p><span style="font-size: medium;"><strong>Mentor:</strong>Sajjadur Rahman Sunny</span></p>
                <p><span style="font-size: medium;">&nbsp;</span></p>
                <p><span style="font-size: medium;">Team BUET BRB was focused on customer satisfaction and overall improvements when it comes to banking services in Bangladesh and came up with &ldquo;Easy Banking Service via Smart App&rdquo;, delivering some unique and innovative features. This application is a cross-platform and stand-alone application, supporting smart phones from Android, iOS, BlackBerry, Windows OS. Using this app, customers can check account balance, bank statements anytime and from anywhere. This app contains loan eligibility checker, which sends customer information to bank server for technical assessment. Moreover, customized ads allow the customer to receive notifications whenever the bank starts a new loan scheme. Apart from that, our Location Based Service (LBS) provides information about the location of all the bank branches and ATM booths. It can also navigate him/her to the nearest ATM booth and branch. </span><br /><span style="font-size: medium;"> Total 59 teams from 20 public and private universities participated in the contest. The contest was held in 3 rounds. The final round was held on 6th October, 2012 in BRAC Centre Inn, Dhaka. 19 teams from BUET participated in the contest. BUET BRB became the 1st runner-up team; Sajjadur Rahman Sunny was the team leader of the team.</span></p>
                <p>&nbsp;</p>
                </tr>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Mobile Based Easy Information Access</strong> (4th CITI Finance IT Case Competition &ndash;3<sup>rd</sup>Runner Up)</span></h1>
                    <div class="row-fluid">                        
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/software/image029.jpg"/>
                        </div>
                        <div class="span4"></div>
                    </div>
                </div>
                <h1><span style="font-size: medium;"><strong>Team Name:</strong> BUET Inferno</span></h1>
                <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
                <ul>
                    <li><span style="font-size: medium;">Tasmin Tamanna Haque (0905065)</span></li>
                    <li><span style="font-size: medium;">Md. Asiful Haque (0905078)</span></li>
                    <li><span style="font-size: medium;">Dastagir Husain Yasin (0905079)</span></li>
                    <li><span style="font-size: medium;">Asif Ud Doula ( 0905090)</span></li>
                </ul>
                <p><span style="font-size: medium;"><strong>Mentor:</strong> Hasan Shahid Ferdous</span></p>
                <p><span style="font-size: medium;">&nbsp;</span></p>
                <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
                <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                <p><span style="font-size: medium;">Team BUET Infernofocused on availability and secure information in view of financial need. And when easy information is to ensure to everyone, mobile phone is the appropriate tool. They designed &ldquo;Mobile Based Easy Information Access&rdquo; system to make the information versatile and easily available.</span></p>
                <p><span style="font-size: medium;">Total 59 teams from 20 public and private universities participated in the contest. The contest was held in 3 rounds. The final round was held on 6th October, 2012 in BRAC Centre Inn, Dhaka. 19 teams from BUET participated in the contest. Amongst the five finalists, three were from BUET. This is actually the continuation of the long standing tradition of triumphs of CSE, BUET teams in CFICC contest. What made the achievement more amazing was that, none of the BUET teams had previous knowledge on neither Information System Design nor Economic Analysis of any financial institution. All the teams were from Level-2, Term-2 and they learned all these by themselves as they crossed various hurdles of the competition. BUET Infernobecame the 2<sup>nd</sup> runner-up team.</span></p>
                </tr>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Help Me</strong> (Samsung Innovation Contest)</span></h1>
                    <div class="row-fluid">
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/software/image031.jpg"/>
                        </div>
                        <div class="span4"></div>
                    </div>
                </div>
                <p><span style="font-size: medium;"><strong>Team name:</strong>BUET-Triangle</span></p>
                <p><strong style="font-size: medium;">Team members</strong><strong>:</strong></p>
                <ul style="font-size: medium;">
                    <li>
                        <p>Saiyma Sarmin(0905048)</p>
                    </li>
                    <li>
                        <p>Maliha Sarwat(0905095)</p>
                    </li>
                    <li>
                        <p>Tasmin Tamanna Haque (0905065)</p>
                    </li>
                </ul>
                <p style="font-size: medium;">Samsung Bangladesh arranged an android development contest in September, 2012. 49 teams from 15 universities of Bangladesh participated in this contest where 27 teams were selected to implement their idea on android platform. Final 7 teams made their journey into the finals where BUET-Triangle presented an app for social impact &lsquo;Help Me&rsquo;. It is developed as an emergency supporting app for android users. This app tracks the user&rsquo;s GPS and finds out the nearby help centers with their contact number and leaves an alert message or call to the nearest one with the user&rsquo;s GPS. </p>
                </tr>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Hall Book</strong> (Inter-BUET CSE Project Contest 2011, category Level-1, Term-ll)</span></h1>
                </div>
                <h1><strong><span style="font-size: medium;">Team Members:</span> </strong></h1>
                <ul>
                    <li><span style="font-size: medium;">Md. Muntakim Sadik (0905003)</span></li>
                    <li><span style="font-size: medium;">Akhter Al-Amin (0905056)</span></li>
                </ul>
                <p><span style="font-size: medium;"><strong>Project Details:</strong></span></p>
                <p><span style="font-size: medium;">Make the book of hall digital must be the main motto of the project &lsquo;Hall Book&rsquo;. Using the programming language C, they make a well featured hall book and it can remove the tiresome process. This kind of useful and well thought app can make ours around easy living.</span></p>
                </tr>
            </table>
            </tr>
            <tr id="10">
            <h1 align="center"><span style="font-size: large;"><strong><span style="text-decoration: underline;">Software Project of BUET- CSE- Batch &rsquo;10</span></strong></span></h1>
            <table>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Online Appointment Manager</strong>(Therap JAVA Fest Winner)</span></h1>
                    <div class="row-fluid">
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/software/image034.jpg"/>
                        </div>
                        <div class="span4"></div>
                    </div>
                </div>
                <h1><span style="font-size: medium;"><strong>Team Name:</strong> BUET ThreeByZero</span></h1>
                <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
                <ul>
                    <li><span style="font-size: medium;">Shamim Hasnath (1005096)</span></li>
                    <li><span style="font-size: medium;">Rafi Kamal (1005003)</span><span style="font-size: medium;"><br /><br /></span></li>
                </ul>
                <p><span style="font-size: medium;">Therap Java Fest was organized by Therap Services for university students all over the country. Interested participants formed a group of two members and submitted a project proposal and then implemented the idea as a Java web application or an Android-based project. After the Therap Java Fest team evaluated the projects, top ranked participants were invited for a live demonstration of their projects on November 14, 2012. BUET ThreeByZero became champion in the contest and won two iPads. They developed an online appointment manager using Java web technology.</span></p>
                </tr>
                <tr>
                <div>
                    <h1><span style="font-size: large;"><strong>Project Name: </strong><strong>Paint and Brush</strong> (Microsoft App-a-thon 2012,Prize in &lsquo;Top Windows Phone App&rsquo;)</span></h1>
                    <div class="row-fluid">
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/software/image032.jpg"/>
                        </div>
                        <div class="span4"></div>
                    </div>
                </div>
                <p>&nbsp;</p>
                <p><span style="font-size: medium;"><strong>Name:</strong>FaysalHossain Shezan(1005076)</span><br /> </p>
                <p><span style="font-size: medium;">Microsoft App-a-thon is an application development marathon for the Windows 8 and Windows phone. Microsoft Bangladesh arranged the program in Institute of Information Technology (IIT), University of Dhaka on November 3, 2012. Faysal Hossain Shezan (1005076) developed an application for windows phone. He made &ldquo;Paint and Brush&rdquo; app and got the top windows phone award. The event was aiming at developing 80 apps but the participants developed 100 apps for the users of Windows 8 and Windows Phones.</span></p>
                </tr>
            </table>
            </tr>
            <tr id="11"></tr>
        </table>
    </div>
</div>