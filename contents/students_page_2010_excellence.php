<div class="row-fluid">
    <table class="table">
        <tbody>
            <tr>
        <div>
            <h1><span style="font-size: large;">Honorable Mention in BITSMUN Pilani 2013</span></h1>
            <img class="img-polaroid" src="assets/images/students/image076.jpg"/>
        </div>
        <p><span style="font-size: medium;">Name: Abdul Kawsar Tushar(1005006)</span></p>
        <p align="center">&nbsp;</p>
        <p style="font-size: medium;">BITSMUN is one of the longest running and most eagerly awaited Model United Nations (MUNs) in India. It has earned the distinction of being one of the best MUNs in the country. The fifth edition was held from 8th to 10th February in Pilani, Rajasthan, India in the campus of Birla Institute of Technology and Science (BITS) - widely regarded as one of the best technical institutes in India for higher education - where Abdul KawsarTushar represented Bangladesh in the committee of World Intellectual Property Organization (WIPO). He received the award of Honorable Mention in the prize giving ceremony from the hands of the Chairman of the Executive Board of WIPO.</p>
        </tr>
        <tr>
        <div>
            <h1><span style="font-size: large;">Chess World Ranking 1810</span></h1>
            <img class="img-polaroid" src="assets/images/students/image078.jpg"/>
        </div>
        <p><span style="font-size: medium;"><strong>Name:</strong>AbhijitMondal(1005075)</span></p>
        <p align="center">&nbsp;</p>
        <p style="font-size: medium;">World Ranking of chess is the standard of determining the rank of a chess player in the whole world. And AbhijitMondal has achieved the World ranking 1810 recently.&nbsp; Besides of this top rank, he has won many national and international contests. Few of his achievements can be mentioned,</p>
        <ul style="font-size: medium;">
            <li>
                <p>Placed 1<sup>st</sup> position in category of rating 1700-1799 at Prime Bank International Rating Tournament 2012, held in November 2012.</p>
            </li>
            <li>
                <p>Placed 4<sup>th</sup>position as member of BUET Chess team at Interuniversity Chess Competition, held in Shah-Jalal Science and Technology University at October 2012.</p>
            </li>
            <li>
                <p>Placed 54<sup>th</sup> position at National &lsquo;B&rsquo; Chess Tournament in September 2012. And this was the topmost position among the Buetians.</p>
            </li>
            <li>
                <p>Placed 26<sup>th</sup> position at Metropolitan Chess Tournament, held in June 2012.</p>
            </li>
            <li>
                <p>Placed 3<sup>rd</sup> position at BUET Chess Championship in April 2012.</p>
            </li>
            <li>
                <p>Places 2<sup>nd</sup>position at BURT Mini Chess Tournament in March 2012.</p>
            </li>
            <li>
                <p>Achieved International FIDA rating 1693.</p>
            </li>
            <li>
                <p>Placed 86<sup>th</sup> position at National &lsquo;B&rsquo; Chess Tournament in October 2011.</p>
            </li>
            <li>
                <p>Placed 8<sup>th</sup> position at National Junior Chess Tournament in September 2011.</p>
            </li>
            <li>
                <p>Placed 3<sup>th</sup> position as member of BUET Chess team at Interuniversity Chess Competition, held in Shah-Jalal Science and Technology University at April 2011.</p>
            </li>
            <li>
                <p>Placed 3<sup>rd</sup> position at BUET Chess Championship in February 2011.</p>
            </li>
        </ul>
        <p>&nbsp;</p>
        </tr>

        <tr>
        <div>
            <h1><span style="font-size: large;">9th in Undergraduate Mathematics Olympiad in 2013</span></h1>]
            <img class="img-polaroid" src="assets/images/students/image080.jpg"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Name:</strong>JannatulFerdauseTumpa(1005047)</span></h1>
        <p>Shahjalal University of Science and Technology arranged the 4th Undergraduate Mathematics Olympiad in 2013. JannatulFerdauseTumpa secured the 9<sup>th</sup> place in that Olympiad.</p>
        </tr>
        </tbody>
    </table>
</div>