<div class="row-fluid">
    <table class="table">
        <thead></thead>
        <tbody>
            <tr>
        <div>
            <h1><span style="font-size: large;">International Robotics Challenge (Runner-up)</span></h1>
            <img class="img-polaroid" src="assets/images/students/image092.jpg"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Team Name</strong>: BUET Skull</span></h1>
        <p><span style="font-size: medium;"><strong>Team Members: </strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Jahidul Islam ('06)</span></li>
            <li><span style="font-size: medium;">Mahfuzul Islam ('07)</span></li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <h5><span style="font-size: medium;"><strong>BUET Skull</strong>took part in the International Robotic Challenge 2013, Techfest- IIT, Bombay. The event was scheduled on January 2-5, 2013. They qualified for the event by winning the regional championship, IRC Bangladesh 2012 on November5, 2012. However, 16 teams including India, Sri Lanka, Egypt, Nepal, France, and Thailand took part in the IRC Grand Finale. They became the Runner up in IRC grand Finale beating all other countries. They lost only against the Champion Sri Lankan team. The team was mentored by Prof. Dr. Md. Monirul Islam, HasanShahidFerdous, NashidShahriar, and Md. MustafizurRahman.</span></h5>
        <p align="center">&nbsp;</p>
        </tr>
        </tbody>
    </table>
</div>