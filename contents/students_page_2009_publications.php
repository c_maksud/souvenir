<div class="row-fluid">
    <table class="table">
        <tr>
        <div>
            <h1><span style="font-size: large;">Poster Acceptance in ACM DEV 2013:</span></h1>
            <center><img class="img-polaroid" src="assets/images/students/image061.jpg"/></center>
        </div>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">About 600 million of world population is illiterate. Due to rapid economic growth a large portion of illiterate mass has access to cell phone. Since most of the operations in typical mobile phones require a minimum level of understanding of the letter written either on the body of the phones or visible on the displays, a natural question raises how these people manage to operate these technical devices. Syed Ishtiaque Ahmed a former faculty of BUET, currently a Fulbright S&amp;T fellow in Cornell University conducted an Ethnographic Study of illiterate people's mobile using pattern along with MarufZaber a student of CSE 09 batch. They explored the techniques and methods that illiterates uses to overcome the literal difficulty while using cell phone. This ethnographic study provided insights of the problems they are facing and hence created a common ground to design user friendly UI for illiterates. Their findings were accepted in ACM DEV 2013 which was held in Bangalore, India.</span></p>
        </tr>
    </table>
</div>