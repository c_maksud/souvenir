<div class="container-fluid">
    <div class="row-fluid">
        <div class="span1"></div>
        <div class="span2">
            <ul class="nav nav-pills nav-stacked" style="position:fixed;">
                <li class="active"><a data-toggle="tab" data-target="#scholarship">Full bright Scholarship</a></li>
                <li><a data-toggle="tab" data-target="#companies">Alumni In Tech Giants</a></li>
                <li><a data-toggle="tab" data-target="#stories">Success Stories</a></li>
            </ul>
        </div>
        <div class="span9">
            <div class="tab-content">
                <div id="scholarship" class="tab-pane active">
                    <div class="row-fluid">
                        <?php
                        $names = array(
                            'Md. Yusuf Sarwar Uddin',
                            'Ahmed Khurshid',
                            'Md Abul Hassan Samee',
                            'Atif Hasan Rahman',
                            'Md. Shamsuzzoha Bayzid',
                            'Anupam Das',
                            'Syed Ishtiaque Ahmed',
                            'Sumaiya Nazeen'
                        );
                        ?>
                        <?php for ($year = 2007, $I = 1; $I<=8; $I++, $year++) { ?>
                                                <div class="span4 well">
                                                    <div class="row-fluid">
                                                        <div class="span12">
                                                            <img class="img-polaroid" src="assets/images/alumni/image00<?php echo $I; ?>.jpg"/>
                                                        </div>
                                                        <div class="span12">
                                                            <h4><?php echo $names[$I - 1]; ?></h4><br/>
                                                            <h4>Year: <?php echo $year; ?></h4>
                                                        </div>
                                                    </div>
                                                </div>
                        <?php } ?>

                    </div>
                </div>
                <div id="companies" class="tab-pane">
                    <h1><span style="font-size: large;">Google</span></h1>
                    <table class="table table-stripped">
                        <thead></thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image009.png"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">01 batch: Istiaque Ahmed</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image010.jpg"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">02 batch: Purujit Saha</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image011.jpg"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">04 batch: Md. Manzurur Rahman Khan (Sidky) </p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image012.gif"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">04 batch: Tanaeem M. Moosa</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image013.jpg"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">’05 batch: Muntasir Mashuq </p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image014.jpg"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">’05 batch: Shahriar Rouf Nafi </p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image015.jpg"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">’07 batch: Md Enzam Hossain</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image016.png" />
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">’07 batch: ABM Faisal Aumy </p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="span6">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <img class="img-polaroid" src="assets/images/alumni/image017.jpg" style="width:172px;height: 155px;"/>
                                            </div>
                                            <div class="span12">
                                                <p style="font-size: medium;">’07 batch: Muhammad Nazmul Hasan Riad</p>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p><span style="font-size: medium;"><a href="http://www.microsoft.com/">Microsoft</a></span></p>
                    <ul>
                        <li><span style="font-size: medium;">Sandeepan Sanyal , Development . MSN , Batch 86</span></li>
                        <li><span style="font-size: medium;">Layek Ali, Testing, Windows Core OS, Batch 86</span></li>
                        <li><span style="font-size: medium;">Sharmin Banu, Testing , Business Monitoring , Batch 88</span></li>
                        <li><span style="font-size: medium;">ATM Shafiqul Khalid, Development, Windows Core OS, Batch 88</span></li>
                        <li><span style="font-size: medium;">Ahsan Khan, Development, MSN, Batch 88</span></li>
                        <li><span style="font-size: medium;">Hossain Touhidur Rahman, Development, MS Office, Batch 89</span></li>
                        <li><span style="font-size: medium;">Mamun Islam, Development, MS Office, Batch 89</span></li>
                        <li><span style="font-size: medium;">Mohammad Meftauddin, Testing, MSN . Batch 91</span></li>
                        <li><span style="font-size: medium;">Abdullah Kavi, Testing, MS Office, Batch 92</span></li>
                        <li><span style="font-size: medium;">Ali Shaiful Alam, Testing Lead, MS Office, Batch 92</span></li>
                        <li><span style="font-size: medium;">Azam Azam, Testing, Exchange Server, Batch 92</span></li>
                        <li><span style="font-size: medium;">Dilshad Akhter, Testing, MS Office, Batch 92</span></li>
                        <li><span style="font-size: medium;">Mohammad Jahangir Hossain, Testing , Windows Media Player, Batch 92</span></li>
                        <li><span style="font-size: medium;">Eshita Sharmin, Testing, Windows Media Player, Batch 93</span></li>
                        <li><span style="font-size: medium;">Farah Farzana , Testing, Windows Core OS , Batch 94</span></li>
                        <li><span style="font-size: medium;">Ferdous Rubaiyat , Testing, Windows Device Experience, Batch 94</span></li>
                        <li><span style="font-size: medium;">Munirul Abedin Pappana. Development , Windows Core OS , Batch 94</span></li>
                        <li><span style="font-size: medium;"><a href="http://www.qicompany.net/Ashfaq">Ashfaq Rahman</a>, Consultant, Windows Longhorn Ehome team, Batch 95Saifur Rahman , Development , MSN , Batch 98</span>
                            <ul>
                                <li>also, CTO,&nbsp;<a href="http://www.qicompany.net/">Qunatitative Intelligence&nbsp;</a>and Computational Linguist,&nbsp;<a href="http://www.vantage.com/">Vantage Labs</a>.</li>
                            </ul>
                        </li>
                        <li></li>
                    </ul>
                    <p><span style="font-size: medium;"><a href="http://www.microsoft.com/">Microsoft Research</a></span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <ul>
                        <ul>
                            <img class="img-polaroid" src="assets/images/alumni/image018.jpg"/>
                            <li><span style="font-size: medium;">Suman Kumar Nath, Batch 91</span></li>
                        </ul>
                    </ul>
                    <p><span style="font-size: medium;">(Three of the graduates got jobs in MICROSOFT before their graduation.)</span></p>
                    <ul>
                        <li><span style="font-size: medium;">Abdullah al Mahmud Satej</span></li>
                        <li><span style="font-size: medium;">Saifur Rahman</span></li>
                        <li><span style="font-size: medium;">Munirul Abedin Pappana</span></li>
                    </ul>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><a href="http://www.intel.com/">Intel</a></span></p>
                    <ul>
                        <li><span style="font-size: medium;">Md. Gazi Salahuddin, Batch 86</span></li>
                        <li><span style="font-size: medium;">Shameem Firoz Akhter (Mithun), Batch 86</span></li>
                        <li><span style="font-size: medium;">Laila Nahar, Batch 86</span></li>
                        <li><span style="font-size: medium;">Touhidur M. Raza, Batch 86</span></li>
                        <li><span style="font-size: medium;">Mohammad M. Miazi, Batch 87</span></li>
                        <li><span style="font-size: medium;">Golam Rasul, Batch 87</span></li>
                        <li><span style="font-size: medium;">Lutfor Rahman Bhuyan, Batch 88</span></li>
                        <li><span style="font-size: medium;">Md. Saffatuddin Quasem, Batch 88</span></li>
                        <li><span style="font-size: medium;">Mohammed Mohibur Rahman, Batch 89</span></li>
                        <li><span style="font-size: medium;">Mohammad Masud Khan, Batch 89</span></li>
                        <li><span style="font-size: medium;">Zaodat Rahman, Batch 89</span></li>
                        <li><span style="font-size: medium;">Mamun Al Mazid, Batch 89</span></li>
                        <li><span style="font-size: medium;">Farzana Akhter, Batch 89</span></li>
                        <li><span style="font-size: medium;">Mohammed Maruf Ahmed, Batch 91</span></li>
                        <li><span style="font-size: medium;">Ahsan Kabeer, Batch 95</span></li>
                        <li><span style="font-size: medium;">Meetesh Barua, Batch 96</span></li>
                    </ul>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="text-decoration: underline; font-size: medium;">Yahoo! Inc.</span></p>
                    <ul>
                        <li><span style="font-size: medium;">Md. Monir Mazumdar, Batch 89</span></li>
                        <li><span style="font-size: medium;">Ahmed Asif Chowdhury, Batch 96</span></li>
                    </ul>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><a href="http://www.ibm.com/">IBM</a></span></p>
                    <ul>
                        <li><span style="font-size: medium;">Tuhin Mahmud, Software Development Engineer, Design Automation, Batch 86,</span></li>
                        <li><span style="font-size: medium;">Ayesha Akhtar, Batch 93</span></li>
                    </ul>
                    <p><span style="text-decoration: underline; font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><a href="http://www.ibm.com/">Facebook</a></span></p>
                    <h1><span style="font-size: medium;">&nbsp;</span></h1>

                    <img class="img-polaroid" src="assets/images/alumni/image019.png"/>
                    <h1><span style="font-size: medium;">&middot;&nbsp;&nbsp;&nbsp; Ishtiaq&nbsp;Hossain</span></h1>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="text-decoration: underline; font-size: medium;">&nbsp;</span></p>
                    <p><span style="text-decoration: underline; font-size: medium;">MathWorks</span></p>
                    <ul>
                        <li><span style="font-size: medium;">Shafquat Rahman, Batch 95</span></li>
                    </ul>
                    <p><span style="text-decoration: underline; font-size: medium;">Panasonic R &amp; D, Japan</span></p>
                    <ul>
                        <li><span style="font-size: medium;">Salahuddin Muhammad Salim Zabir (Joy), Batch 87</span></li>
                    </ul>
                    <p><span style="font-size: medium;"><a href="http://www.motorola.com/">Motorola</a></span></p>
                    <ul>
                        <li><span style="font-size: medium;">Md. Anisuzzaman, Batch 87</span></li>
                        <li><span style="font-size: medium;">Tarik Mahmood, Batch 88</span></li>
                        <li><span style="font-size: medium;">Zulfiqer Sekender, Batch 89</span></li>
                        <li><span style="font-size: medium;">Syed Masum Emran, Batch 90</span></li>
                        <li><span style="font-size: medium;">Amalendu Roy, Batch 91</span></li>
                        <li><span style="font-size: medium;">Mohammad Aminul Haq, Batch 94</span></li>
                    </ul>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><strong>Faculty Members in renowned Universities:</strong></span></p>
                    <ul>
                        <li><span style="font-size: medium;"><a href="http://www.ece.uwaterloo.ca/People/faculty/hasan.html">M. Anwar Hasan</a>, Professor, ECE Dept,&nbsp;<a href="http://www.uwaterloo.ca/">University of Waterloo</a>, Canada</span></li>
                        <li><span style="font-size: medium;"><a href="http://www.gscit.monash.edu.au/gscitweb/index.php?type=i&amp;id=5">M Manzur Murshed</a>, Professor at&nbsp;<a href="http://www.monash.edu.au/">Monash University</a>&nbsp;, Australia (Batch 87)</span></li>
                        <li><span style="font-size: medium;"><a href="http://www.gscit.monash.edu.au/gscitweb/index.php?type=i&amp;id=4">Gour C. Karmakar</a>, Senior Lecturer, Monash University, Australia (Batch 86)</span></li>
                        <li><span style="font-size: medium;">Manoranjan Paul, Research fellow, School of ITEE, University of New South Wales</span></li>
                        <li><span style="font-size: medium;"><a href="http://www.ee.umanitoba.ca/%7Eekram/">AZM Ekram Hossain</a>, Professor at the&nbsp;<a href="http://www.umanitoba.ca/">University of Manitoba</a>&nbsp;, Canada</span></li>
                        <li><span style="font-size: medium;"><a href="http://www.utdallas.edu/%7Elkhan/">Latifur Khan</a>, Professor at the&nbsp;<a href="http://www.utdallas.edu/">University of Texas at Dallas</a></span></li>
                        <li><span style="font-size: medium;"><a href="http://www.mscs.mu.edu/%7Eiq/">Sheikh Iqbal Ahamed</a>, Associate professor at the&nbsp;<a href="http://www.mu.edu/">Marquette University</a>&nbsp;, Wisconsin</span></li>
                        <li><span style="font-size: medium;"><a href="http://www.cs.queensu.ca/home/mzulker/">Mohammad Zulkernine</a>, Associate Professor,&nbsp;<a href="http://www.queensu.ca/">Queens University</a>, Canada</span></li>
                        <li><span style="font-size: medium;">Mohammad A. Aleem, Quinsigamond Community College, Worcester, Massachusetts</span></li>
                        <li><span style="font-size: medium;">Ragib Hasan, Assistant Professor, CSE, <a href="http://www.google.com/url?sa=t&amp;rct=j&amp;q=university%20of%20alabama%20at%20birmingham&amp;source=web&amp;cd=1&amp;cad=rja&amp;sqi=2&amp;ved=0CC8QFjAA&amp;url=http%3A%2F%2Fwww.uab.edu%2F&amp;ei=p8wPUa7VJoSjigfF74GoDA&amp;usg=AFQjCNGjZw4a0qOSHgn4CmO6VtlIxUM_sw&amp;bvm=bv.41867550,d.aGc"><em>University of Alabama at Birmingham</em></a></span></li>
                    </ul>
                </div>
                <div id="stories" class="tab-pane">
                    <img class="img-polaroid" src="assets/images/alumni/image020.jpg"/>
                    <p><span style="font-size: medium;"><strong><span style="text-decoration: underline;">3SM System</span></strong></span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>    
                    <p><span style="font-size: medium;">Hasan Shihab Uddin, Sujoy Kumar Chowdhury, Nahid Mahfuza Alam and Md. Mahbubur Rahman, four students of CSE department, developed Bangla SMS system, named &ldquo;3SM System&rdquo;. This was the first ever Bangla SMS system in Bangladesh. The Pacific Bangladesh Telecom Ltd. (CityCell) commercially launched this system in their various Value Added Services and millions of subscribers are getting service from it.</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>


                    <p><span style="font-size: medium;"><strong><span style="text-decoration: underline;">Musical Heart</span></strong></span></p>
                    <img class="img-polaroid" src="assets/images/alumni/image022.jpg"/>
                    <p><span style="font-size: medium;">A University of Virginia graduate student has developed a biofeedback-based system that helps smartphones select music that will</span></p>
                    <p><span style="font-size: medium;">A University of Virginia graduate student has developed a biofeedback-based system that helps smartphones select music that will help get their owners&rsquo; heart pumping during exercise, or slow it down when they want to cool down or relax.</span></p>
                    <p><span style="font-size: medium;">&ldquo;Whether I am driving, jogging, traveling or relaxing &ndash; I never find the appropriate music to listen to,&rdquo; said Shahriar Nirjon, a doctoral student in&nbsp;<a href="http://www.cs.virginia.edu/"><strong>computer science</strong></a>&nbsp;in the&nbsp;<a href="http://seas.virginia.edu/"><strong>School of Engineering and Applied Science</strong></a>. &ldquo;I believe there are many like me. The problem is: The heart wants to hear something, but our music player does not understand the need. My joy was in connecting them together &ndash; in a non-invasive and cost-effective way.&rdquo;</span></p>
                    <p><span style="font-size: medium;">Called &ldquo;Musical Heart,&rdquo; the system &ldquo;brings together wellness and entertainment,&rdquo; Nirjon said.</span></p>
                    <p><span style="font-size: medium;">Musical Heart works by merging a microphone that detects the pulse in arteries in the ear with earphones that bring in music from a playlist on a smartphone. An app selects tunes that optimize the heart rate of an individual user based on a given activity, whether running, walking or relaxing &ndash; playing fast-paced music for hard workouts, and slowing the beat for cool-downs. An algorithm refines the music selection process of the system by storing heart rate data and calculating the effects of selected music on the rate. Over time, it improves music selections to optimize the user&rsquo;s heart rate.</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>

                    <p><span style="font-size: medium;"><strong>Dr. Manzur M. Murshed</strong><strong>:</strong></span></p>
                    <img class="img-polaroid" src="assets/images/alumni/image023.jpg"/>
                    <p><span style="font-size: medium;">Dr. Manzur M. Murshed is a distinguished alumnus of the CSE Department, BUET. Throughout his academic career, Dr. Murshed has achieved distinction. He studied in Chittagong Collegiate School, and later in Chittagong College. He stood first in both the SSC and HSC examinations under Comilla Board. After graduation from BUET at the top of his class (Batch 87) , he received his PhD degree in Computer Science from the Australian National University, Canberra, Australia, in 1999. He is currently an Associate Professor and the Director of Research at Gippsland School of Information Technology, Monash University, Australia, where his major research interests are in the fields of Video Coding and Transcoding, Video Indexing and Retrieval, Video-on-Demand, Image Processing, Multimedia Communications, Wireless Communications, Parallel and Distributed Computing, Grid Computing, Simulation, Complexity Analysis, Multilingual Systems, Algorthms, Distributed Coding, and Digitial Watermarking. He has published 106 journal and peer-reviewed research publications. He is the recipient of numerous academic awards including the University Gold Medal by BUET. He is a member of the IEEE.</span></p>

                    <p><span style="font-size: medium;"><strong>Dr. Latifur Khan:</strong></span></p>
                    <img class="img-polaroid" src="assets/images/alumni/image024.jpg"/>
                    <p><span style="font-size: medium;">Professor, Department of Computer Science, Erik Jonsson School of Engineering and Computer Science, University of Texas at Dallas.</span></p>
                    <p><span style="font-size: medium;">Dr. Latifur Khan is an ACM Distinguished Scientist and a Senior Member of IEEE. He is currently a full Professor in the Computer Science department at the University of Texas at Dallas where he has been teaching and conducting research since September 2000. He received his Ph.D. and M.S. degrees in Computer Science from the University of Southern California in August of 2000, and December of 1996 respectively. He has received prestigious awards including the IEEE Technical Achievement Award for Intelligence and Security Informatics. He has given several keynote addresses including at the IEEE International Conference on Tools with Artificial Intelligence ICTAI 2010 in Arras, France; Third IEEE ICDM International Workshop on Semantic Aspects in Data Mining SADM 2010, Sydney, Australia; IEEE International Conference on Sensor Networks, Ubiquitous, and Trustworthy Computing 2010, Newport Beach, California; and Pacific Asia Knowledge Discovery in Databases 2012 in Kuala Lumpur Malaysia.</span></p>
                    <p><span style="font-size: medium;">Dr. Khan has over 170 published papers in 40 journals. His papers have been published in VLDB, Journal of Web Semantics, Elsevier, IEEE TDKE, IEEE TDSC, IEEE TSMC Bioinformatics Journal, AI Research, and several other leading journals; in prestigious conferences such as IEEE ICDM, ACM Multimedia, AAAI, IJCAI, ACM WWW, ACM GIS, ICWC, IEEE ICSC, IEEE Cloud, INFOCOM and leading conferences ECML/PKDD, PAKDD, ICTAI, ACM SACMAT. In addition, Dr. Khan has published two books based on his students' theses.</span></p>
                    <p><span style="font-size: medium;">Dr. Khan's research areas are data mining, multimedia information management and semantic web and database systems. He has served as a committee member in many prestigious conferences, symposiums and workshops including ACM SIGKDD Conference on Knowledge Discovery and Data Mining, IEEE International Conference on Data Mining (ICDM), Pacific-Asia Conference on Knowledge Discovery and Data Mining (PAKDD), and SIAM International Conference on Data Mining (SDM).</span></p>
                    <p><span style="font-size: medium;">This great Computer Scientist obtained his B.Sc. degree in Computer Science and Engineering from Bangladesh University of Engineering and Technology, Dhaka, Bangladesh in November of 1993 with First class Honors (2nd position). He received Chancellor Awards from the President of Bangladesh.</span></p>

                    <p><span style="font-size: medium;"><strong>Dr. Ekram Hossain:</strong></span></p>
                    <img class="img-polaroid" src="assets/images/alumni/image025.jpg"/>
                    <p><span style="font-size: medium;">Dr. Ekram Hossain graduated from CSE, BUET in the year of 1995.&nbsp; Since March 2010, Dr. Hossain is a Professor in the Department of Electrical and Computer Engineering at University of Manitoba, Winnipeg, Canada. He received his Ph.D. in Electrical Engineering from University of Victoria, Canada, in 2001. He was a University of Victoria Fellow and also a recipient of the British Columbia Advanced Systems Institute (ASI) graduate student award. Professor Hossain is an author/editor of the books <em>Smart Grid Communications and Networking</em> (Cambridge University Press, 2012), <em>Green Radio Communication Networks</em> (Cambridge University Press, 2012), <em>Cooperative Cellular Wireless Networks</em> (Cambridge University Press, 2011), <em>Dynamic Spectrum Access and Management in Cognitive Wireless Networks</em> (Cambridge University Press, 2009), <em>Heterogeneous Wireless Access Networks</em> (Springer, 2008), <em>Cognitive Wireless Communication Networks</em> (Springer, 2007), <em>Wireless Mesh Networks: Architectures and Protocols</em> (Springer, 2007), and <em>Introduction to Network Simulator NS2</em> (Springer, 2007). Currently Dr. Hossain serves as the <em>Editor-in-Chief</em> for the <em>IEEE Communications Surveys and Tutorials</em> and an Editor for the <em>IEEE Journal on Selected Areas in Communications - Cognitive RAdio Series</em> and <em>IEEE Wireless Communications</em>. Also, he serves on the IEEE Press Editorial Board (for the term 2013-2015). Previously, Dr. Hossain served as the <em>Area Editor</em> for the <em>IEEE Transactions on Wireless Communications</em> (in the area of "Resource Management and Multiple Access") from 2009-2011 and an Editor for the <em>IEEE Transactions on Mobile Computing</em> (from 2007-2012). He is a Distinguished Lecturer for the IEEE Communications Society (for the term 2012-2013). Dr. Hossain is a registered Professional Engineer (P.Eng.) in the province of Manitoba, Canada.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>

                    <p><span style="font-size: medium;"><strong>Dr. Mohammad Zulkernine:</strong></span></p>
                    <img class="img-polaroid" src="assets/images/alumni/image026.jpg"/>
                    <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;">Mohammad Zulkernine is a Canada Research Chair (Tier 2) in Software Dependability and an Associate Professor at the School of Computing of Queen&rsquo;s University, Canada. He leads the Queen&rsquo;s Reliable Software Technology (QRST) research group. In 2009-10, he was a Visiting Professor of the University of Trento, Italy. Dr. Zulkernine received his BSc from Bangladesh University of Engineering and Technology, MEng form Muroran Institute of Technology, Japan, and PhD from University of Waterloo, Canada. Dr. Zulkernine&rsquo;s current research focuses on software reliability and security. His research projects are funded by a number of provincial and federal research organizationsof Canada, while he is collaborating through various industrial research partnerships. Dr. Zulkernine is a senior member of the IEEE, a member of the ACM, and a licensed professional engineer in the province of Ontario, Canada.</span></p>
                </div>
            </div>
        </div>
    </div>
</div>