<div class="container-fluid">
    <div class="row-fluid">
        <table class="table table-bordered" style="font-size: medium;">
            <tbody>
                <tr>
                    <td>
                        <p align="center">Name</p>
                    </td>
                    <td  >
                        <p align="center">Designation</p>
                    </td>
                    <td  >
                        <p align="center">Achievements</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="5"  >
                        <p>Dr. Md. Kaykobad</p>
                    </td>
                    <td rowspan="5"  >
                        <p>Professor</p>
                    </td>
                    <td  >
                        <p>Academy Gold Medal for senior scientists in physical science in 2004.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Director, ACM ICPC Asia Region Dhaka Site, 2001, 2002, 2003</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Gold Medal for contribution in ICT Educationby Bangladesh Computer Society, in 2005.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Editor of <strong>&ldquo;Madhyamik Computer Shikhsha&rdquo;, </strong>Text Book for classes IX and X, 1996.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>&ldquo;<strong>Computer Programming</strong>&rdquo;, a textbook for students of Bangladesh Open University, in 1997.</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="4"  >
                        <p>Dr. Md. Saidur Rahman</p>
                    </td>
                    <td rowspan="4"  >
                        <p>Professor</p>
                    </td>
                    <td  >
                        <p>Academy Gold Medal for junior scientists in physical science in 2003.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>FUNAI Information Technology Award in 2004.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>UGC Award administered by University Grants Commission, Bangladesh in 2004.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Written a book <em>&ldquo;Planar Graph Drawing&rdquo;</em> with Takao Nishizeki.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Dr. Md. Monirul Islam</p>
                    </td>
                    <td  >
                        <p>Professor</p>
                    </td>
                    <td  >
                        <p>3 awards (including best paper) at SCIS and ISIS in 2006.</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3"  >
                        <p>Dr. MasudHasan</p>
                    </td>
                    <td rowspan="3"  >
                        <p>Professor</p>
                    </td>
                    <td  >
                        <p>TWAS-BAS Young Scientist Award by TWAS, Italy in 2011.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>International Graduate Student Award by University of Waterloo, Canada in September 2001 &ndash; August 2005.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Teaching Assistant at University of Waterloo, Canada from September 2001 to September 2005</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Dr. Mohammad Mahfuzul Islam</p>
                    </td>
                    <td  >
                        <p>Professor</p>
                    </td>
                    <td  >
                        <p>President ,Bangladesh Computer Society</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3"  >
                        <p>Dr. Md. Sohel Rahman</p>
                    </td>
                    <td rowspan="3"  >
                        <p>Associate Professor</p>
                    </td>
                    <td  >
                        <p>Academy Gold Medal for junior scientists in physical science in 2008.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>UGC Award, administered by University Grants Commission, Bangladesh in 2010.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Editor of &ldquo;<em>Texts in Algorithmics&rdquo;</em>, Volume 11 by Joseph Wun-Tat Chan, Jackie Daykin</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2"  >
                        <p>Dr. Reaz Ahmed</p>
                    </td>
                    <td rowspan="2"  >
                        <p>Associate Professor</p>
                    </td>
                    <td  >
                        <p>Fred W. Ellersick Prize Paper Award by IEEE communication society in 2008.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p><em>&ldquo;Distributed Search and Pattern Matching</em><em>, Handbook of Peer-to-peer Networking&rdquo;</em> with R. Boutaba in April, 2009.</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="4"  >
                        <p>&nbsp;Dr. Mohammed Eunus Ali</p>
                    </td>
                    <td rowspan="4"  >
                        <p>Associate Professor</p>
                    </td>
                    <td  >
                        <p>CoreDB Research Award, University of Melbourne, Australia in 2010<strong>.</strong></p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Best Popular Poster Award, EII 2009.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>EII Publication Prize Award for ICDE 2008 Paper.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Works as Data Analysis Consultant at <em>Fleet Software and Services, </em>Melbourne, Australia</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="3"  >
                        <p>&nbsp;Dr. A.K.M. AshikurRahman</p>
                    </td>
                    <td rowspan="3"  >
                        <p>Associate Professor</p>
                    </td>
                    <td  >
                        <p>Author of the book <em>&ldquo;Routeless Routing and MAC-Aided Protocols for Adhoc Wireless Network&rdquo;</em></p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Werner Von Siemens Excellence Award, 1998.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>&nbsp;2004/2005&nbsp;Ph.D. Research Award.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Dr. Md. Yusuf Sarwar Uddin</p>
                    </td>
                    <td  >
                        <p>Assistant Professor</p>
                    </td>
                    <td  >
                        <p>Recipient of the &ldquo;<em>International Fulbright Science and Technology Fellowship</em>&rdquo; in 2007</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="4"  >
                        <p>Dr. Md. Shohrab Hossain</p>
                    </td>
                    <td rowspan="4"  >
                        <p>Assistant Professor</p>
                    </td>
                    <td  >
                        <p>OU GSS&nbsp;<strong><em>Overall Outstanding Graduate Student Award</em></strong>&nbsp;(University-wide) 2011-2012.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>IEEE ICC Student Travel Grant ,2012</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>IEEE MILCOM Student Travel Grant ,2010</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>&nbsp;</p>
                        <p>CS Graduate Fellowship for the 2012-2013 academic year in University of Oklahoma</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="4"  >
                        <p>S.M. Farhad</p>
                    </td>
                    <td rowspan="4"  >
                        <p>Assistant Professor</p>
                    </td>
                    <td  >
                        <p><strong>Google Publication Prize 2011</strong>&nbsp;was awarded for "Orchestration by Approximation, Mapping Stream Programs onto Multi-Core Architectures", received in May 2012</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p><strong>Research Excellence 2011</strong>&nbsp;award in recognition of outstanding research achievement in PhD Program at the Faculty of Engineering and Information Technologies, The University of Sydney, received in 2011</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p><strong>Usyd-Is</strong>&nbsp;scholarship has been awarded from The University of Sydney on March 2009 for PhD Program</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p><strong>NICTA</strong>&nbsp;scholarship has been awarded from The University of Sydney on March 2009 for PhD Program</p>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2"  >
                        <p>&nbsp;Md. Faizul Bari&nbsp;</p>
                    </td>
                    <td rowspan="2"  >
                        <p>Assistant Professor</p>
                    </td>
                    <td  >
                        <p>International Doctoral Student Award, University of Waterloo, in 2010-2011.</p>
                    </td>
                </tr>
                <tr>
                    <td  >
                        <p>Mathematics Graduate Experience Award, University of Waterloo.</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
