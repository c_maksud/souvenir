<div class="row-fluid">
    <table class="table">
        <thead></thead>
        <tbody>
            <tr>
        <div>
            <h1><span style="font-size: large;">4th CITI Finance IT Case Competition &ndash;1<sup>st</sup> Runner Up</span><br /><span style="font-size: medium;"> <strong>Team Name:</strong> BUET BRB</span></h1>
            <img class="img-polaroid" src="assets/images/students/image050.jpg"/>
        </div>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">TaslimArefin Khan (0905045)</span></li>
            <li><span style="font-size: medium;">Muhammad Hossain Mahdi (0905008)</span></li>
            <li><span style="font-size: medium;">Mehrab Bin Morshed (0905022)</span></li>
            <li><span style="font-size: medium;">SadiaNahreen (0905006)</span></li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">Team BUET BRB focused on customer satisfaction and overall improvements when it comes to banking services in Bangladesh and came up with &ldquo;Easy Banking Service via Smart App&rdquo;, delivering some unique and innovative features. This application is a cross-platform and stand-alone application, supporting smart phones from Android, iOS, BlackBerry, Windows OS. Using this app, customers can check account balance, bank statements anytime and from anywhere. This app contains loan eligibility checker, which sends customer information to bank server for technical assessment. Moreover, customized ads allow the customer to receive notifications whenever the bank starts a new loan scheme. Apart from that, our Location Based Service (LBS) provides information about the location of all the bank branches and ATM booths. It can also navigate him/her to the nearest ATM booth and branch. </span><br /><span style="font-size: medium;"> Total 59 teams from 20 public and private universities participated in the contest. The contest was held in 3 rounds. The final round was held on 6th October, 2012 in BRAC Centre Inn, Dhaka. 19 teams from BUET participated in the contest. BUET BRB became the 1st runner-up team; SajjadurRahman Sunny was the team leader of the team.</span></p>
        </tr>
        <tr>
        <div>
            <h1><span style="font-size: large;">4th CITI Finance IT Case Competition - 3<sup>rd</sup> Runner Up</span></h1>
            <img class="img-polaroid" src="assets/images/students/image052.jpg"/>
        </div>
        <h1><br /><span style="font-size: medium;"> <strong>Team Name:</strong> BUET Inferno</span></h1>
        <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">TasminTamannaHaque (0905065)</span></li>
            <li><span style="font-size: medium;">Md. AsifulHaque (0905078)</span></li>
            <li><span style="font-size: medium;">Dastagir Husain Yasin (0905079)</span></li>
            <li><span style="font-size: medium;">AsifUd Doula (0905090)</span></li>
        </ul>
        <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">Citi Bank arranged a programming contest for young and talented minds from different public and private universities of Bangladesh to demonstrate their unique talent in software and information system development for an ever demanding financial sector.Total 59 teams from 20 public and private universities participated in the contest. The contest was held in 3 rounds. The final round was held on 6th October,2012 in BRAC Centre Inn, Dhaka. 19 teams from BUET participated in the contest. BUET Inferno became the 3rd runner-up team; HasanShahidFerdous was the team leader of the team.</span></p>
        </tr>
        <tr>
        <div>
            <h1><span style="font-size: large;">BSADD JAVA Contest Champion</span></h1>
            <img class="img-polaroid" src="assets/images/students/image054.png"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Team Name:</strong> BUET BRB</span></h1>
        <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">SadiaNahreen (0905006)</span></li>
            <li><span style="font-size: medium;">TaslimArefin Khan (0905045)</span></li>
            <li><span style="font-size: medium;">Muhammad Hossain Mahdi (0905008)</span></li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">BUET Systems Analysis, Design &amp; Development Group (BSADD) organized a System Development Contest on several tracks like Java, PHP, ASP.NET etc. Team BRB consisting of Muhammad Hussain Mahdi, TaslimArefin Khan and SadiaNahreen from batch &rsquo;09 participated on the Java track and became the Champion.</span></p>
        <p><span style="font-size: medium;">There were several other teams from &rsquo;09 batch. There were teams from batch &rsquo;08 and &rsquo;10 as well. The jurors consisted of faculty members and experts in relevant field working in reputed software companies.</span></p>
        <p><span style="font-size: medium;">The contest problem asked the contestants to design and implement a system which is a common prototype used in day to day baking services. The judgementcriteria was based on efficient implementation of database, database design, concept and implementation.</span></p>
        </tr>
        <tr>
        <div>
            <h1><span style="font-size: large;">Sanitation Hackathon</span></h1>

        </div>
        <h1><span style="font-size: medium;"><strong>Team name:</strong>BUET Nocturnes</span></h1>
        <img class="img-polaroid" src="assets/images/students/image058.png"/>
        <p><span style="font-size: medium;"><strong>Team members</strong><strong>:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Akhter Al Amin Farhan (0905056)</span></li>
            <li><span style="font-size: medium;">Muhammad Hossain Mahdi (0905008)</span></li>
            <li><span style="font-size: medium;">MalihaSarwat(0905095)</span></li>
            <li><span style="font-size: medium;">Mehrab Bin Morshed(0905022)</span></li>
            <li><span style="font-size: medium;">Mohammad Rakib Amin(0905046)</span></li>
            <li><span style="font-size: medium;">SadiaNahreen(0905006)</span></li>
            <li><span style="font-size: medium;">SaiymaSarmin(0905048)</span></li>
            <li><span style="font-size: medium;">ShouravNath (0905030)</span></li>
            <li><span style="font-size: medium;">SaidurRahmanSujon(0905111)</span></li>
            <li><span style="font-size: medium;">TasminTamannaHaque (0905065)</span></li>
        </ul>
        <p><span style="font-size: large;">&nbsp;</span></p>
        <p><span style="font-size: medium;">Hackathon is a marathon program where coders hack computer programs to solve critical problems. Sanitation Hackathon in Dhaka held from November 30<sup>th</sup> to December 1<sup>st</sup>, 2012 sponsored by <strong>World Bank</strong> was a <strong>36 hours long marathon</strong> where coders, developers, planners and highly experienced professionals in IT sector from all corners of Bangladesh and the world were gathered and had to solve real life problems regarding sanitation worldwide. The team BUET Nocturnes (Farhan, Mahdi, Maliha, Mehrab, Rakib, Sadia, Sayma, Shourav, Sujon, Tamanna) brought a complete and dedicated system to step against pollution due to effluent matters. Among all 46 solutions created there on that night theirs was selected as one of the best six solutions.</span></p>
        </tr>
        <tr>
        <div>
            <h1><span style="font-size: large;">Samsung Innovation Contest:</span></h1>
            <center><img class="img-polaroid" src="assets/images/students/image060.jpg"/></center>
        </div>
        <p><span style="font-size: large;">&nbsp;</span></p>
        <p><span style="font-size: medium;"><strong>Team name:</strong>BUET-Triangle</span></p>
        <p><span style="font-size: medium;"><strong>Team members</strong><strong>:</strong></span></p>
        <ul>
            <li>
                <p><span style="font-size: medium;">SaiymaSarmin(0905048)</span></p>
            </li>
            <li>
                <p><span style="font-size: medium;">MalihaSarwat(0905095)</span></p>
            </li>
            <li>
                <p><span style="font-size: medium;">TasminTamannaHaque (0905065)</span></p>
            </li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">Samsung Bangladesh arranged an android development contest in September, 2012. 49 teams from 15 universities of Bangladesh participated in this contest where 27 teams were selected to implement their idea on android platform. Final 7 teams made their journey into the finals where BUET-Triangle presented an app for social impact &lsquo;Help Me&rsquo;. It is developed as an emergency supporting app for android users. This app tracks the user&rsquo;s GPS and finds out the nearby help centers with their contact number and leaves an alert message or call to the nearest one with the user&rsquo;s GPS.</span></p>
        </tr>
        </tbody>
    </table>
</div>