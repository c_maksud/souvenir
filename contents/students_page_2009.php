<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-pills">
            <li class="active"><a data-target="#2009_contests" data-toggle="tab">Software Competition</a></li>
            <li><a data-target="#2009_acm" data-toggle="tab">Programming Comtests</a></li>
            <li><a data-target="#2009_publications" data-toggle="tab">Research Publications</a></li>
        </ul>
        <div class="tab-content">
            <div id="2009_contests" class="tab-pane active">
                <?php include_once 'students_page_2009_contests.php';?>
            </div>
            <div id="2009_acm" class="tab-pane">
                <?php include_once 'students_page_2009_acm.php';?>
            </div>
            <div id="2009_publications" class="tab-pane">
                <?php include_once 'students_page_2009_publications.php';?>
            </div>
        </div>
    </div>
</div>