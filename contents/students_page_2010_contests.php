<div class="row-fluid">
    <table class="table">
        <thead></thead>
        <tbody>
            <tr>
        <div>
            <h1><span style="font-size: large;">Microsoft App-a-thon 2012:Prize in &lsquo;Top Windows Phone App&rsquo;</span></h1>
            <img class="img-polaroid" src="assets/images/students/image064.jpg"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Name:</strong>FaysalHossainShezan(1005076)</span></h1>
        <p><span style="font-size: medium;">Microsoft App-a-thon is an application development marathon for the Windows 8 and Windows phone. Microsoft Bangladesh arranged the program in Institute of Information Technology (IIT), University of Dhaka on November 3, 2012. FaysalHossainShezan (1005076) developed an application for windows phone. He made &ldquo;Paint and Brush&rdquo;&nbsp; app and got the top windows phone award. The event was aiming at developing 80 apps but the participants developed 100 apps for the users of Windows 8 and Windows Phones.</span></p>
        </tr>
        <tr>
        <div>
            <h1><span style="font-size: large;">Therap JAVA Fest Winner</span></h1>
            <img class="img-polaroid" src="assets/images/students/image066.jpg"/>
        </div>
        <p><strong>Team Name:</strong> BUET ThreeByZero</p>
        <h1><span style="font-size: medium;"><strong>Team members:</strong></span></h1>
        <ul>
            <li><span style="font-size: medium;">ShamimHasnath (1005096)</span></li>
            <li><span style="font-size: medium;">Rafi Kamal (1005003)</span><br /><br /></li>
        </ul>
        <p><span style="font-size: medium;">Therap Java Fest was organized by Therap Services for university students all over the country. Interested participants formed a group of two members and submitted a project proposal and then implemented the idea as a Java web application or an Android-based project. After the Therap Java Fest team evaluated the projects, top ranked participants were invited for a live demonstration of their projects on November 14, 2012. BUET ThreeByZero became champion in the contest and won two iPads. They developed an online appointment manager using Java web technology.</span></p>
        </tr>
        </tbody>
    </table>
</div>