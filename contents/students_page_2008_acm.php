<div class="row-fluid">
    <table class="table">
        <tr id="icpc-dhaka-regional">
        <div>
            <h1><span style="font-size: large;">ICPC Dhaka Regional 2012 Runner-up &amp;QuaziAzher Ali SAARC Programming Contest 2013 Champion:</span></h1>
            <img class="img-polaroid" src="assets/images/students/image003.jpg"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Team Name:&nbsp;&nbsp; Choker</strong></span></h1>
        <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Muhammad NazmulHasanRiad(0705014)</span></li>
            <li><span style="font-size: medium;">PrasanjitBarua(0805090)</span></li>
            <li><span style="font-size: medium;">Mohammad Hafiz Uddin(0805004)</span></li>
        </ul>
        <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">The ICPC Regional Asia-Dhaka site contest 2012 was conducted by Daffodil International University. This large regional contest was held with a presence of number of foreign teams from China and India and a total of 149 teams. Team Choker solved seven problems and became runner up in the contest and also the champion among the Bangladeshi teams. They are qualified for the ICPC World Finals 2013 which will be held in St Petersburg, Russia.&nbsp;&nbsp;</span></p>
        <p><span style="font-size: medium;">Bangladesh University arranged <strong>QuaziAzher Ali SAARC Programming Contest(QAASPC)2013 .</strong>This contest was held with a presence of number of foreign teams from Nepal and India and a total of 86 teams. Team Choker solved seven problems and became champion in the contest.</span></p>
        <p><span style="font-size: medium;">Choker also became champion in Daffodil Pre-ACM Programming Contest 2011, BUET IUPC 2011, BUBT National Programming Contest 2012.</span></p>
        </tr>
        <tr id="QuaziAzhar">
        <div>
            <h1><span style="font-size: large;">QuaziAzhar Ali National Programming Contest 2010 Champion:</span></h1>
            <img class="img-polaroid" src="assets/images/students/image005.jpg"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Team Name: </strong>BhushondirKak</span></h1>
        <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Md. MahbubulHasan(05)</span></li>
            <li><span style="font-size: medium;">ShahriarNouf Rafi (05)</span></li>
            <li><span style="font-size: medium;">KonokHabibullah Arafat(0805106)<br /></span><span style="font-size: medium;"> <br /></span></li>
        </ul>
        <p><span style="font-size: medium;">Bangladesh University arranged <strong>QuaziAzher Ali National Programming Contest2010 .</strong>This contest was held with a presence of around 60 teams. Team BhushondirKak solved seven problems and became champion in the contest.</span></p>
        </tr>
        <tr id="third_icpc">
        <div>
            <h1><span style="font-size: large;">3<sup>rd</sup> in ICPC Regional Asia-Dhaka site contest 2012 and 5<sup>th</sup> in QAASPC 2013</span></h1>
            <div class="row-fluid">
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image006.jpg"/>
                </div>
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image008.jpg"/>
                </div>
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image010.jpg"/>
                </div>
            </div>
        </div>
        <h1><span style="font-size: medium;"><strong>Team Name:</strong> Kay Dynasty</span></h1>
        <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Md. Kaysar Abdullah(0805059)</span></li>
            <li><span style="font-size: medium;">Radi Muhammad Reza(0805002)</span></li>
            <li><span style="font-size: medium;">SakibShafayat(0905062)</span></li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">The ICPC Regional Asia-Dhaka site contest 2012 was conducted by Daffodil International University. Team Kay Dynasty obtained 3<sup>rd</sup> position among 147 Bangladeshi teams. Bangladesh University arranged QuaziAzher Ali SAARC Programming Contest(QAASPC) 2013 . This contest was held with a presence of number of foreign teams from Nepal and India and a total of 86 teams. Kay Dynasty solved six problem and obtained 5<sup>th</sup> position. Kay Dynasty was in top 5 in several other contests, BGC TRUST programming contest 2012, QAAPC 2012.</span></p>
        </tr>
        <tr id="">
        <div>
            <h1><span style="font-size: large;">National Collegiate Programming Contest (NCPC) 2011 Runner-up (Women&rsquo;s contest)</span></h1>
            <div class="row-fluid">
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image018.png"/>
                </div>
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image056.png"/>
                </div>
            </div>
        </div>
        <h1><span style="font-size: medium;"><strong>Team name:</strong> BUET Hyperion</span></h1>
        <p><span style="font-size: medium;"><strong>Team members</strong><strong>:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">TamzidaTarannum (0805035)</span></li>
            <li><span style="font-size: medium;">SamihaSamrose (0805066)</span></li>
            <li><span style="font-size: medium;">SadiaNahreen(0905006)</span></li>
        </ul>
        <p><span style="font-size: medium;">National Collegiate Programming Contest (NCPC) arranged a programming contest for women programmers in Dhaka University on 3<sup>rd</sup> June, 2011. A total number of 45 teams consisting of only women programmers from different universities of Bangladesh participated in this event. &lsquo;BUET Hyperion&rsquo; was the only team representing BUET and it became the runner-up team. Md. MahbubulHasanShanto, the coach, and ShahriarRoufNafiguided the team.</span></p>
        </tr>
        <tr id="yellow-coder">
        <div>
            <h1><span style="font-size: large;">Yellow Coder in TopCoder:</span></h1>
        </div>
        <ol>
            <div class="row-fluid">
                <div class="span8">
                    <li><span style="font-size: medium;">Nahiyan Kamal (0805006)</span></li>
                </div>
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image095.jpg"/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8">
                    <li><span style="font-size: medium;">PrasanjitBarua(0805090)</span></li>
                </div>
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image034.jpg"/>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8">
                    <li><span style="font-size: medium;">Mohammad Hafiz Uddin(0805005)<br /><br /></span></li>
                </div>
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image036.jpg"/>
                </div>
            </div>            
        </ol>
        </tr>
        <tr>
        <h1><span style="font-size: large;">Top Problem Solver of Bangladesh in Timus Online Judge</span></h1>
        <div class="row-fluid">
            <div class="span8">
                <span style="font-size: medium;">Radi Muhammad Reza (0805002): World Ranking 66.<br /><br /></span
            </div>
            <div class="span4">
                <img class="img-polaroid" src="assets/images/students/image038.jpg"/>
            </div>
        </div>
        </tr>    
    </table>
</div>