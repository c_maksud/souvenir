<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-pills">
            <li class="active"><a data-target="#2011_acm" data-toggle="tab">Programming Comtests</a></li>
            <li><a data-target="#2011_excellence" data-toggle="tab">Excellence</a></li>
        </ul>
        <div class="tab-content">
            <div id="2011_acm" class="tab-pane active">
                <?php include_once 'students_page_2011_acm.php';?>
            </div>
            <div id="2011_excellence" class="tab-pane">
                <?php include_once 'students_page_2011_excellence.php';?>
            </div>
        </div>
    </div>
</div>