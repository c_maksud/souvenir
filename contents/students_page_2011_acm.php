<div class="row-fluid">
    <table class="table">
        <tr>
        <div>
            <h1><span style="font-size: large;">Bangladesh Informatics Olympiad 2012: Grab 3<sup>rd</sup>, 6<sup>th</sup>, 7<sup>th</sup>and 10<sup>th</sup> place</span></h1>
            <div class="row-fluid">
                <div class="span3">
                    <img class="img-polaroid" src="assets/images/students/image082.jpg"/>
                </div>
                <div class="span3"><img class="img-polaroid" src="assets/images/students/image084.jpg"/></div>
                <div class="span3"><img class="img-polaroid" src="assets/images/students/image086.jpg"/></div>
                <div class="span3"><img class="img-polaroid" src="assets/images/students/image088.jpg"/></div>
            </div>
        </div>
        <p><span style="font-size: medium;"><strong>Name:Rank 03</strong>- KaziHasanZubaer(1105024)</span></p>
        <p><span style="font-size: medium;"><strong>Name:Rank 06</strong>- Mir ImtiazMostafiz(1105002)</span></p>
        <p><span style="font-size: medium;"><strong>Name:Rank 07</strong>- AmlanSaha (1105054)</span></p>
        <p><span style="font-size: medium;"><strong>Name:Rank 10</strong>- EunaMehnaz Khan (1105046)<br /> <br /></span></p>
        <p><span style="font-size: medium;">On January 20, 2012, the Divisional Informatics Olympiad, 2012 took place at Bangladesh University of Engineering and Technology. The participants who were participated in the competition and elected few were allowed to attend the national one. The National Informatics Olympiad, 2012 was held at Ahsanullah University of Science and Technology on February 3, 2012. They were given approximately 3 hours to solve eleven problems. They had to solve the problems individually. After the competition and lunch break, the prize giving ceremony started. Professor A.M.M. Safiullah, Vice Chancellor of AUST and&nbsp;Dr. M. Kaykobad, Professor at BUET, were the honorable guests in the ceremony and inspired the participants for programming through their speech.&nbsp;Four students of BUET CSE batch&rsquo;11 placed position 3<sup>rd</sup> (KaziHasanZubaer), 6<sup>th</sup> (Mir ImtiazMostafiz), 7<sup>th </sup>(AmlanSaha) and 10<sup>th</sup> (EunaMehnaz Khan) among top ten positions.</span></p>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        </tr>
    </table>
</div>