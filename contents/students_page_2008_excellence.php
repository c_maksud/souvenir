<div class="row-fluid">
    <table class="table">
        <tr id="avhijit">
        <div>
            <h1><span style="font-size: large;">National Math Olympiad in SUST:</span></h1>
            <img class="img-polaroid" src="assets/images/students/image028.png"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Champion:</strong>AvijitSarker(0805009)</span></h1>
        <p><span style="font-size: medium;">Shahjalal University of Science and Technology arranged the 4th Undergraduate Mathematics Olympiad in 2013. AvijitSarker secured the first place in that Olympiad. He scored 65 out of 80.</span></p>
        </tr>
        <tr id="mahdi">
        <div>
            <h1><span style="font-size: large;">Mahdi MashrurIbneMatin</span> </h1>
            <div class="row-fluid">
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image030.jpg"/>
                </div>
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image032.jpg"/>
                </div>
            </div>
        </div>
        <p><span style="font-size: large;"><br /> 1) &nbsp;Represented Team-BUET in Worlds University Debate Championship-2012(Manila, Philippines), dubbed as the Olympics /world finals of debating.</span><br /><span style="font-size: large;"> Scored a staggering 10 points</span><br /> <br /> <br /><span style="font-size: large;"> 2) 3<sup>rd</sup> ranking EFL (English as Foreign Language) speaker of asia in 2011 NSU ASIAN British Parliamentary Debate Championship</span><br /> <br /><span style="font-size: large;"> 3) Highest speaker points and team points in NALSAR Intervarsity Debate Championship-2011(Hyderabad, India) from Bangladesh.</span><br /> <br /><span style="font-size: large;"> 4) Interfaculty English Debate Championship-2010 champion </span><br /> <br /><span style="font-size: large;"> 5) Breaking debater of BUET National Debate Championship-2011 , EWU Debate Spree-2011 &amp; many others.</span><br /> <br /><span style="font-size: large;"> 6)Chief Adjudicator for BUET InterSchool English Debate Championship-2012</span><br /> <br /><span style="font-size: large;"> 7)&nbsp; Nationally 7<sup>th</sup> ranked in the NSU Intervarsity Math Olympiad-2010.&nbsp; Honorable mansion awards of SUST Intervarsity Math Olympiad-2013 &amp; EWU Intervarsity Math Olympiad-2012</span><br /> <br /> <br /><span style="font-size: large;"> 8)Best performer of comedy &amp; magic in the Worlds` Comedy Night-2012 (Philippines).</span></p>
        </tr>
    </table>
</div>