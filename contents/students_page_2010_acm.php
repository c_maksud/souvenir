<div class="row-fluid">
    <table class="table">
        <tr>
        <div>
            <h1><span style="font-size: large;">9<sup>th</sup> in ACM ICPC 2012 Dhaka Regional</span><strong><br /> <span style="font-size: medium;">Team Members:</span></strong></h1>
            <div class="row-fluid">
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image068.jpg"/>
                </div>
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image070.jpg"/>
                </div>
            </div>
        </div>
        <ul>
            <li><span style="font-size: medium;">Jamshed Khan Mukut(1005012)</span></li>
            <li><span style="font-size: medium;">Ahamed Al Nahian(1005002)</span></li>
            <li><span style="font-size: medium;">MoinulShaon(1005015)</span></li>
        </ul>
        <p><span style="font-size: medium;">ACM International Collegiate Programming Contest (ACM ICPC) is an annual multi-tiers competitive programming competition among universities of the world. Each year winner of the Asia Regional Final Dhaka Site Contest advances to the ACM-ICPC World Final. ACM ICPC 2012 was organized by Daffodil International University.&nbsp; The location of the contest was Grand Ball Room of Radisson Blue Water Garden Hotel. Total 146 teams from the country and 3 other foreign participated in this event. In this competition, brightest minds from the country and outside were brainstorming, trying to outdo each other through logic and keyboard. The excitement was irresistible.</span></p>
        </tr>
        <tr>
        <div>
            <h1><span style="font-size: large;">16<sup>th</sup> in ACM ICPC 2012 Dhaka Regional</span></h1>
            <div class="row-fluid">
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image072.jpg"/>
                </div>
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image074.jpg"/>
                </div>
            </div>
        </div>
        <h1><span style="font-size: medium;"><strong>Team Name: </strong>BUET_Raikiri</span></h1>
        <p><span style="font-size: medium;"><strong> Team Members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Mohammad UllahFarsid (1005093)</span></li>
            <li><span style="font-size: medium;">SadiaAtique (1005013)<br /></span><span style="font-size: medium;"> <br /></span></li>
        </ul>
        <p><span style="font-size: medium;">ACM International Collegiate Programming Contest (ACM ICPC) is an annual multi-tiers competitive programming competition among universities of the world. Each year winner of the Asia Regional Final Dhaka Site Contest advances to the ACM-ICPC World Final. ACM ICPC 2012 was organized by Daffodil International University.&nbsp; The location of the contest was Grand Ball Room of Radisson Blue Water Garden Hotel. Total 146 teams from the country and 3 other foreign participated in this event. In this competition, brightest minds from the country and outside were brainstorming, trying to outdo each other through logic and keyboard. The excitement was irresistible. BUET_Raikiri solved 4 problems out of 11.</span></p>
        </tr>
    </table>
</div>