<div class="row-fluid">
    <table class="table">
        <tr>
        <h1><span style="font-size: large;">#Papers:</span></h1>
        <ul>
            <li>Md. RidwanurRahman, ProteekChandan Roy, Shujon Naha, Ishrat Ahmed(0805064), SamihaSamrose(0805066), Md. MizanurRahman, Syed Ishtiaque Ahmed, <strong>&ldquo;A-Class: Intelligent Classroom Software for the Autistic Children&rdquo;</strong>, in the proceedings of The IEEE Symposium on Computer and Informatics (ISCI) , Penang, Malaysia, March 2011, IEEE Computer Society Press, pp. 727-731.</li>
        </ul>
        <p>&nbsp;</p>
        <ul>
            <li>Md. RashidujjamanRifat(0805107), ShubramiMoutushy(0805082), H. S. Ferdous, <strong>&ldquo;A Location Based Advertisement Scheme using OpenStreetMap&rdquo;</strong>, in the proceedings of International Conference on Computer and Information Technology (ICCIT 2012), December 22-24, 2012, Chittagong, Bangladesh.</li>
        </ul>
        <p>&nbsp;</p>
        <ul>
            <li>H. S. Ferdous, FarhanaMurtazaChoudhury , Md. RashidujjamanRifat(0805107), ShubramiMoutushy(0805082),&nbsp; <strong>&ldquo;Usability Analysis of e-Governance Services in Bangladesh - A Survey and Future Directions&rdquo;</strong>, in the proceedings of International Conference on Computer and Information Technology (ICCIT 2012), December 22-24, 2012, Chittagong, Bangladesh.</li>
        </ul>
        <p>&nbsp;</p>
        <ul>
            <li>Md. RashidujjamanRifat(0805107), ShubramiMoutushy(0805082), Syed Ishtiaque Ahmed, Hasan S. Ferdous, <strong>&ldquo;Location Based Information System Using OpenStreetMap&rdquo;</strong>, in the proceedings of The IEEE Student Conference on Research and Development (SCORED 2011), December 2011, Malaysia. pp. 397-402.<br /><br /><br /></li>
            <li>Md. RashidujjamanRifat(0805107), ShubramiMoutushy(0805082), Syed Ishtiaque Ahmed, HasanShahidFerdous, <strong>"Location Based Information System Using OpenStreetMap"</strong>, in Proc. ICAEE'2011, IUB, Bangladesh.</li>
        </ul>
        <p>&nbsp;</p>
        <ul>
            <li>RidwanurRahman, Md. RashidujjamanRifat(0805107), ShubramiMoutushy(0805082), HasanShahidFerdous,<strong>"Towards a Customizable Effective Patient Management System for&nbsp;Ensuring Quality Medical Service"</strong>, in the proceedings of The IEEE Student Conference on Research and Development (SCORED 2011), December 2011, Malaysia.<br /><br /></li>
        </ul>
        <ul>
            <li>Anika Anwar(0805083), Md. MustafizurRahman, S. M. Ferdous, SamiulAlamAnik,&nbsp;Syed Ishtiaque Ahmed, <strong>"A Computer Game based Approach for Increasing Fluency in the&nbsp; Speech of the Autistic Children", </strong>in the proceedings of The IEEE International Conference on Advanced Learning Technologies (ICALT), Athens, Georgia, USA, July 2011, IEEE Computer Society Press.&nbsp;pp. 17-18.<br /><br /></li>
            <li>HimelDev(0805021), TanmoySen(0805058), MadhusudanBasak(0805039), &nbsp;Mohammed Eunus Ali, "<strong>An Approach to Protect the Privacy of Cloud Data from Data Mining Based Attacks</strong>", to appear in in DataCloud 2012 (SC12).</li>
        </ul>
        <p>&nbsp;</p>
        <h1><span style="font-size: large;">#Journals:</span></h1>
        <ul>
            <li>Md. MustafizurRahman, S. M. Ferdous, Syed Ishtiaque Ahmed, AnikaAnwar (0805083), <strong>"Speech Development of Autistic Children by Interactive Computer Games"</strong>, International Journal of Interactive Technology and Smart Education (ITSE), Emerald. Vol. 8(4): pp.208-223, 2011.</li>
        </ul>
        <p>&nbsp;</p>
        <h1><span style="font-size: large;">#Newsletters:</span></h1>
        <ul>
            <li>Md. RashidujjamanRifat(0805107), ShubramiMoutushy(0805082), HasanShahidFerdous,<strong>'Guess or Locate: A Location based Gaming Approach to Teach Children Collaborating with the Real World'</strong>,accepted at IEEE Learning Technology Newsletter, April 2012 issue.</li>
        </ul>
        <p>&nbsp;</p>
        <ul>
            <li>FarhanaMurtazaChoudhury,Md. RashidujjamanRifat(0805107), ShubramiMoutushy(0805082),HasanShahidFerdous,<strong>'RetrospectGallery: Create Stories from&nbsp;&nbsp;&nbsp;Your&nbsp;Life&rsquo;</strong> at IEEE Learning Technology Newsletter, April 2012 issue.</li>
        </ul>
        <h1><span style="font-size: large;">#Reviewer:</span></h1>
        <ul>
            <li>Reviewer of IEEE Colloquium on Humanities, Science &amp; Engineering Research&rsquo;2012.Md. RashidujjamanRifat(0805107), ShubramiMoutushy (0805082)<br /><br /></li>
            <li>Reviewer in 2012 IEEE Symposium on Business, Engineering and Industrial Applications (ISBEIA 2012) Md. RashidujjamanRifat(0805107),</li>
        </ul>
        </tr>
    </table>
</div>