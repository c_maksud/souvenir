<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-pills">            
            <li class="active"><a data-target="#2008_contests" data-toggle="tab">Software Competition</a></li>
            <li><a data-target="#2008_acm" data-toggle="tab">Programming Comtests</a></li>
            <li><a data-target="#2008_publications" data-toggle="tab">Research Publications</a></li>
            <li><a data-target="#2008_excellence" data-toggle="tab">Excellence</a></li>
        </ul>
        <div class="tab-content">
            <div id="2008_contests" class="tab-pane active">
                <?php include_once 'students_page_2008_contests.php'; ?>
                            </div>
                            <div id="2008_acm" class="tab-pane">
                <?php include_once 'students_page_2008_acm.php'; ?>
                            </div>                
                            <div id="2008_publications" class="tab-pane">
                <?php include_once 'students_page_2008_publications.php'; ?>
                            </div>
                            <div id="2008_excellence" class="tab-pane">
                <?php include_once 'students_page_2008_excellence.php'; ?>
            </div>
        </div>
    </div>
</div>