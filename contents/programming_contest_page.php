<div class="container-fluid">
    <div class="row-fluid">
        <div class="span1"></div>
        <div class="span2">
            <ul class="nav nav-pills nav-stacked" style="position:fixed;">
                <li class="active"><a data-toggle="tab" data-target="#finalists">Finalists</a></li>
                <li><a data-toggle="tab" data-target="#stories">Success Stories</a></li>
                <li><a data-toggle="tab"  data-target="#summary">Summary</a></li>
            </ul>
        </div>
        <div class="span8">
            <div class="tab-content">
                <div id="finalists" class="tab-pane active">
                    <center>
                        <div class="span6">
                            <div id="myCarousel" class="carousel slide">
                                <!-- Carousel items -->
                                <div class="carousel-inner">
                                    <div class="active item">
                                        <img src="assets/images/acm/image001.jpg" >
                                        <div class="carousel-caption">
                                            <h4>BUET Team 1998</h4>
                                        </div>

                                    </div>
                                    <?php for ($year = 1999, $I = 2; $I<=15; $I++, $year++) {
                                    ?>
                                    <?php if ($I < 10): ?>
                                    <?php $name = 'image00' . $I . '.jpg' ?>
                                    <?php else: ?>
                                    <?php $name = 'image0' . $I . '.jpg' ?>
                                    <?php endif; ?>
                                                                        <div class="item">
                                                                            <img src="assets/images/acm/<?php echo $name; ?>" >
                                                                            <div class="carousel-caption">
                                                                                <h4>BUET Team <?php echo $year; ?></h4>
                                            <?php if ($year=='2000'): ?>
                                                                                        <h4>    BUET Team 2000 (ranked 11th in the Final, so far the best performance of BUET)</h4>
                                            <?php endif; ?>
                                                                                    </div>

                                                                                </div>


                                    <?php } ?>
                                    <div class="item">
                                        <img src="assets/images/acm/image019.png" >
                                        <div class="carousel-caption">
                                            <h4>2013 Finalist</h4>
                                        </div>
                                    </div>
                                </div>
                                <!-- Carousel nav -->
                                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                            </div>
                        </div>
                    </center>
                </div>
                <script type="text/javascript">
                    $('.carousel').carousel({
                        interval: 5000
                    });
                </script>
                <div id="stories" class="tab-pane">
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="row-fluid">
                                <div class="span12">
                                    <img src="assets/images/acm/image020.jpg" >
                                </div>
                                <div class="span12">
                                    <p><span style="font-size: medium;">Suman Kumar Nath</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="row-fluid">
                                <div class="span12">
                                    <img src="assets/images/acm/image021.jpg" >
                                </div>
                                <div class="span12">
                                    <p><span style="font-size: medium;">Rezaul Alam Chowdhury</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="row-fluid">
                                <div class="span12">
                                    <img src="assets/images/acm/image022.jpg" style="width: 214px;height: 235px">
                                </div>
                                <div class="span12">
                                    <p><span style="font-size: medium;">Tarique Mesbaul Islam</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;">Suman Kumar Nath (now at Microsoft Research), Rezaul Alam Chowdhury (now faculty of Stony Brook University) and Tarique Mesbaul Islam (now at IBM, Toronto) with the team name &ldquo;Bengal Tigers&rdquo; participated in the 22<sup>nd</sup> World Final of ACM-ICPC held on 28 February, 1998 in Hotel Marriott Marquis and occupied 24<sup>th</sup> position among 54 teams and tied with the famous Stanford University.</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>               

                    <p><span style="font-size: medium;">BUET has never failed to qualify for the World Finals of ACM-ICPC since it started participation in 1998. There are only few universities in the world that have such a long streak of uninterrupted participation in the World Finals of ACM-ICPC.</span></p>
                    <p><span style="font-size: medium;">The best rank of BUET is 11<sup>th</sup> in the 24<sup>th</sup> World Final of ACM-ICPC held at Orlando, Florida, USA on 18<sup>th</sup> March, 2000. The team outperformed teams from MIT, Harvard, Stanford, Virginia Tech, Cornell, Georgia Tech and other famous universities.</span></p>
                    <p><span style="font-size: medium;">Out of 15 times, BUET was ranked 9 times. So far maximum number of problems a BUET team solved is 4 in years 2000, 2005, 2008, 2009 and 2010. The best timing for BUET team solving 4 problems is 185 minutes at Stockholm in 2009.</span></p>
                    <p><span style="font-size: medium;">So far the youngest finalist from BUET is Shahriar Rouf Nafi. He was 19 years and 137 days old on the day of his first World Final.</span></p>
                    <center>
                        <div class="row-fluid">
                            <div class="span12">
                                <img src="assets/images/acm/image023.jpg" >
                            </div>
                            <div class="span12">
                                <p><span style="font-size: medium;">&nbsp;<strong>Shahriar Rouf Nafi</strong></span></p>
                            </div>
                        </div>
                    </center>
                    <p><span style="font-size: medium;">Informed sources remarked that execution time of the solution provided by BUET team in the World Final 2010 of ACM-ICPC for problem J (Sharing Chocolate) was the fastest among submitted solutions.</span></p>
                    <p><span style="font-size: medium;">BUET also hosted regional contests for the Dhaka site several times.</span></p>
                    <p><span style="font-size: medium;">Shahriar Manzoor and Rezaul Alam Chowdhury, graduates of this department, have been playing a leading role in hosting international programming contests.</span></p>
                    <p><span style="font-size: medium;">CSE BUET graduates have also achieved success in the organizational area of ACM-ICPC. Shahriar Manzoor of CSE Batch &lsquo;94 has the honor of being selected an&nbsp;<a href="http://icpc.baylor.edu/dmt/view_image.asp?i=16775">ACM-ICPC World Final judge</a>&nbsp;for four consecutive years (2003-2006). Shahriar Manzoor and another graduate of CSE Batch &lsquo;97 Md. Kamruzzaman are members of the Elite Problem Setters&rsquo; Panel of&nbsp;<a href="http://online-judge.uva.es/contest/">Valladolid Online Judge</a>. This site received an award from ACM for its contribution in the training and popularization of programming contests around the world.</span></p>
                    <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                    <div class="row-fluid">
                        <div class="span12">
                            <p align="center"><img src="assets/images/acm/image024.jpg" ></p>
                        </div>
                        <div class="span12"><p align="center"><span style="font-size: medium;"><strong>Dr.&nbsp;</strong><a href="http://www.buet.ac.bd/cse/faculty/facdetail.php?id=kaykobad"><strong>Mohammad Kaykobad</strong></a></span></p></div>
                    </div>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;">Prof.&nbsp;<a href="http://www.buet.ac.bd/cse/faculty/facdetail.php?id=kaykobad">M. Kaykobad</a>&nbsp;has led the BUET team as its coach to the World Finals&nbsp;seven&nbsp;times in a row, an achievement surpassed by few coaches. He was awarded the&nbsp;<a href="http://www.acm.inf.ethz.ch/ICPC02/tour/images/22142414.jpg">Best Coach Award</a> in 2002 ACM ICPC World Final in Honolulu.</span></p>
                </div>
                <div id="summary" class="tab-pane">
                    <p><span style="font-size: medium;">&nbsp;CSE department of BUET has enormous success in various national and international programming contests. The department team has been participating in the prestigious world final of ACM (Association for Computing Machinery) International Collegiate Programming Contest (ACM-ICPC) since the year 1998.</span></p>
                    <p><span style="font-size: medium;">ACM is the largest CS community of the world. To inspire computer skills among young people, ACM organizes International Collegiate Programming Contest every year. BUET students first participated in ACM-ICPC world final in the year 1998. Since then this has been a great success story of consistent performance by BUET students in world finals.</span></p>
                    <p><span style="font-size: medium;">Following table summarizes the programming contest performance of the department team in different World Finals of ACM-ICPC.&nbsp; Here the list is arranged according to the order of place in the ACM ICPC Finals that a BUET team achieved.</span></p>
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;"><strong>ACM Finals</strong></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;"><strong>Date</strong></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;"><strong>Venue</strong></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;"><strong>Team</strong></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;"><strong>Place</strong></span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">24<sup>th</sup></span></p>
                                    <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">18.03.2000</span></p>
                                    <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Orlando,</span></p>
                                    <p align="center"><span style="font-size: medium;">Florida,</span></p>
                                    <p align="center"><span style="font-size: medium;">USA</span></p>
                                    <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Backtrackers</span></p>
                                    <p><span style="font-size: medium;">Mustaq Ahmed</span><br /><span style="font-size: medium;"> Munirul Abedin</span><br /><span style="font-size: medium;"> Rubaiyat Ferdous Jewel</span></p>
                                    <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">11</span></p>
                                    <p align="center"><span style="font-size: medium;">&nbsp;</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">22<sup>nd</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">28.02.1998</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Atlanta,</span></p>
                                    <p align="center"><span style="font-size: medium;">Georgia,</span></p>
                                    <p align="center"><span style="font-size: medium;">USA</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">Bengal Tigers</span></p>
                                    <p><span style="font-size: medium;">Suman Kumar Nath</span><br /><span style="font-size: medium;"> Rezaul Alam Chowdhury</span><br /><span style="font-size: medium;"> Tarique Mesbaul Islam</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">24</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">28<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">31.03.2004</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Prague,</span></p>
                                    <p align="center"><span style="font-size: medium;">Czech Republic</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Phoenix</span></p>
                                    <p><span style="font-size: medium;">Asif-ul Haque</span><br /><span style="font-size: medium;"> Mohammad Saifur Rahman</span><br /><span style="font-size: medium;"> Mehedi Bakht</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">27</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">25<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">10.03.2001</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Vancouver,</span></p>
                                    <p align="center"><span style="font-size: medium;">Canada</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Loopers</span></p>
                                    <p><span style="font-size: medium;">Mustaq Ahmed</span><br /><span style="font-size: medium;"> Munirul Abedin</span><br /><span style="font-size: medium;"> Abdullah Al Mahmood</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">29</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">29<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">06.04.2005</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Shanghai,</span></p>
                                    <p align="center"><span style="font-size: medium;">China</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Explorer</span></p>
                                    <p><span style="font-size: medium;">Mushfiqur Rouf Nasa</span><br /><span style="font-size: medium;"> Abdullah Al Mahmud</span><br /><span style="font-size: medium;"> Manzurur Rahman Khan</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">29</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">&nbsp;32<sup>nd</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">04-06.04.2008</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Bannf,</span></p>
                                    <p align="center"><span style="font-size: medium;">Alberta,</span></p>
                                    <p align="center"><span style="font-size: medium;">Canada</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Sprinter</span></p>
                                    <p><span style="font-size: medium;">Shahriar Rouf</span><br /><span style="font-size: medium;"> Md. Mahbubul Hasan</span><br /><span style="font-size: medium;"> Sabbir Yousuf Sanny (EEE)</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">31</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">33<sup>rd</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">21.04.2009</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">StockHolm,</span></p>
                                    <p align="center"><span style="font-size: medium;">Sweden</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Falcon</span></p>
                                    <p><span style="font-size: medium;">Tanaeem M Moosa</span><br /><span style="font-size: medium;"> Shahriar Rouf</span><br /><span style="font-size: medium;"> Md. Mahbubul Hasan</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">34</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">34<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">05.02.2010</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Harbin,</span></p>
                                    <p align="center"><span style="font-size: medium;">China</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Rand Ecliptic</span></p>
                                    <p><span style="font-size: medium;">Tanaeem M Moosa</span><br /><span style="font-size: medium;"> Tasnim Imran Sunny</span><br /><span style="font-size: medium;"> Muntasir Mashuq</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">36</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">30<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">09.04.2006</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">San Antonio,</span></p>
                                    <p align="center"><span style="font-size: medium;">Texas,</span></p>
                                    <p align="center"><span style="font-size: medium;">USA</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Exceed</span></p>
                                    <p><span style="font-size: medium;">Istiaque Ahmed</span><br /><span style="font-size: medium;"> Manzurur Khan</span><br /><span style="font-size: medium;"> Omar Haider</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">39</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">26<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">23.03.2002</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Honolulu,</span></p>
                                    <p align="center"><span style="font-size: medium;">Hawaii,</span></p>
                                    <p align="center"><span style="font-size: medium;">USA</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Ackermanns</span></p>
                                    <p><span style="font-size: medium;">Abdullah Al Mahmood</span><br /><span style="font-size: medium;"> Md. Kamruzzaman</span><br /><span style="font-size: medium;"> Mushfiqur Rouf Nasa</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">H.M.</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">27<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">25.03.2003</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Beverly Hills,</span></p>
                                    <p align="center"><span style="font-size: medium;">California,</span></p>
                                    <p align="center"><span style="font-size: medium;">USA</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Loopers</span></p>
                                    <p><span style="font-size: medium;">Asif-ul Haque</span><br /><span style="font-size: medium;"> Mohammad Saifur Rahman</span><br /><span style="font-size: medium;"> Mehedi Bakht</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">H.M.</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">23<sup>rd</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">12.04.1999</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Eindhoven,</span></p>
                                    <p align="center"><span style="font-size: medium;">Netherlands</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">The Baloon Counters</span></p>
                                    <p><span style="font-size: medium;">Rezaul Alam Chowdhury</span><br /><span style="font-size: medium;"> Mojahedul Hoque Abul Hasnat</span><br /><span style="font-size: medium;"> Mohammad Mehedy Masud</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">H.M.</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">31<sup>st</sup></span></p>
                                </td>
                                <td>
                                    <p align="center"><span style="font-size: medium;">15.03.2007</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Tokyo,</span></p>
                                    <p align="center"><span style="font-size: medium;">Japan</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET xC33d</span></p>
                                    <p><span style="font-size: medium;">Istiaque Ahmed</span><br /><span style="font-size: medium;"> Sabbir Yousuf Sanny (EEE)</span><br /><span style="font-size: medium;"> Mohammad Mahmudur Rahman (EEE)</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">H.M.</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">35<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">30.05.2011</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Orlando,</span></p>
                                    <p align="center"><span style="font-size: medium;">Florida</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET Annihilator</span><br /><span style="font-size: medium;"> Tasnim Imran Sunny</span><br /><span style="font-size: medium;"> Muntasir Mashuq</span></p>
                                    <p><span style="font-size: medium;">Anindya Das</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">H.M.</span></p>
                                </td>
                            </tr>
                            <tr>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">36<sup>th</sup></span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">17.05.2012</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">Warsaw,</span></p>
                                    <p align="center"><span style="font-size: medium;">Poland</span></p>
                                </td>
                                <td  >
                                    <p><span style="text-decoration: underline; font-size: medium;">BUET .oO</span></p>
                                    <p><span style="font-size: medium;">Mir Wasi Ahmed</span><br /><span style="font-size: medium;"> Md. Enzam Hossain</span><br /><span style="font-size: medium;"> F. A. Rezaur Rahman Choudhury</span></p>
                                </td>
                                <td  >
                                    <p align="center"><span style="font-size: medium;">H.M.</span></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
                    <p><span style="font-size: medium;">* H.M. = Honorable Mention</span></p>
                </div>
            </div>
        </div>
    </div>
</div>