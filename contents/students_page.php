<div class="container-fluid">
    <div class="row-fluid">
        <div class="span1"></div>
        <div class="span2">
            <ul class="nav nav-pills nav-stacked" style="position:fixed;">
                <li class="active"><a data-target="#2008" data-toggle="tab">2008</a></li>
                <li><a class="divider"></a></li>
                <li><a data-target="#2009" data-toggle="tab">2009</a></li>
                <li><a class="divider"></a></li>
                <li><a  data-target="#2010" data-toggle="tab">2010</a></li>
                <li><a class="divider"></a></li>
                <li><a  data-target="#2011" data-toggle="tab">2011</a></li>
                <li><a class="divider"></a></li>
                <li><a  data-target="#msc" data-toggle="tab">Msc</a></li>
                <li><a class="divider"></a></li>
            </ul>
        </div>
        <div class="span8">
            <div class="tab-content">
                <div id="2008" class="tab-pane active">
                    <?php include_once 'students_page_2008.php';?>
                </div>
                <div id="2009" class="tab-pane">
                    <?php include_once 'students_page_2009.php';?>
                </div>
                <div id="2010" class="tab-pane">
                    <?php include_once 'students_page_2010.php';?>
                </div>
                <div id="2011" class="tab-pane">
                    <?php include_once 'students_page_2011.php';?>
                </div>
                <div id="msc" class="tab-pane">
                    <?php include_once 'students_page_msc.php';?>
                </div>
            </div>
        </div>
    </div>
</div>