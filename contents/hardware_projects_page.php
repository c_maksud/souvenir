<div class="container-fluid">
    <div class="row-fluid">
   
        <div class="span2">
            <ul class="nav nav-pills nav-stacked" style="position:fixed;">
                <li class="active"><a data-toggle="tab" data-target="#blink-walk">Blind Walk - Tool For Blind People</a></li>
                <li><a data-toggle="tab" data-target="#road-side">SMART ROADSIDE INDICATOR</a></li>
                <li><a data-toggle="tab" data-target="#toll-collection">AUTOMATED TOLL COLLECTION SYATEM</a></li>
                <li><a data-toggle="tab" data-target="#token-both">Smart Token Booth ,Queue Management System</a></li>
                <li><a data-toggle="tab" data-target="#water-facet">Automated Water Faucet</a></li>
                <li><a data-toggle="tab" data-target="#sms-home">SMS BASED HOME APPLIANCE</a></li>
            </ul>
        </div>
        <div class="span2"></div>
        <div class="span8">
            <div class="tab-content">
                <div id="blink-walk" class="tab-pane active">      
                    <h1><span style="font-size: medium;">Blind Walk - Tool For Blind People</span></h1>
                    <div class="row-fluid">
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/hardware/image001.png"/>
                        </div>
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/hardware/image002.png"/>
                        </div>
                    </div>
                    <h1><span style="font-size: large;"><strong>Project Members:</strong></span></h1>
                    <ol style="font-size: medium;">
                        <li>Md. Ibrahim Rashid-07</li>
                        <li>Abm Faisal -07</li>
                        <li>KaziNasirUddin -07</li>
                        <li>Tarequl Islam Sifat -07</li>
                        <li>Ashiq Imran-07</li>
                    </ol>
                    <img class="img-polaroid" src="assets/images/hardware/image004.png"/>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><strong>Project Description:</strong></span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Under this project a devicehas been build thatcan detect nearby obstacles and warn the user (blind) before head using beeping sound. Thus this device will be very helpful for blind people to move around much more easily.</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><strong>Project Feature:</strong></span><br /><span style="font-size: medium;"> 1.There are 3 sensors towards straight, left &amp;right; covering approximately 180 degree in front.</span></p>
                    <p><span style="font-size: medium;">2.&nbsp;&nbsp;&nbsp; It returns feedback with sound &amp; vibration.</span></p>
                    <p><span style="font-size: medium;">3.&nbsp;&nbsp;&nbsp; It can detect obstacle upto 3 meters distance.</span></p>
                    <p><span style="font-size: medium;">4.&nbsp;&nbsp; There are two different modes.</span></p>
                    <p><span style="font-size: medium;">&nbsp;5.&nbsp;&nbsp;&nbsp; Observation mode : All three sensors are turned on.</span></p>
                    <p><span style="font-size: medium;">6.&nbsp;&nbsp;&nbsp;&nbsp; Free mode : Only front sensor is active.</span></p>
                    <p><span style="font-size: medium;">7.&nbsp;&nbsp;&nbsp; Has rechargeable battery and can be recharged with charger.</span></p>
                    <p><span style="font-size: medium;">8.&nbsp; Any 3.5 mm headphone can be connected with the device.</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                </div>
                <div id="road-side" class="tab-pane">
                    <h1 style="font-size: large;"><span style="font-size: medium;">SMART ROADSIDE INDICATOR</span></h1>
                    <img class="img-polaroid" src="assets/images/hardware/image005.jpg"/>
                    <p><span style="font-size: medium;"><strong>Project Description: </strong></span></p>
                    <p><span style="font-size: medium;">As we stroll down the streets, we see frequent roadside indicator boards with specific messages on them- like the speed limit, or speedbreaker ahead, Do not blow horn and so on. But when we are busy driving, we can miss these signs, and sometimes, the result is disastrous. So we thought, why not take this to another level? So here we are, with our audible traffic signs, which will tell you whatever the indiator board is trying to say.</span></p>
                    <p><br clear="ALL" /><span style="font-size: medium;"> <strong>Featuring Image:</strong></span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
                    <p><span style="font-size: medium;"><strong>Project Participant: </strong></span></p>
                    <p><span style="font-size: medium;">1.Kazi Tasnif Islam, batch 07, Department of CSE, BUET</span></p>
                    <p><span style="font-size: medium;">&nbsp;2.&nbsp; Md.Zahidul Islam, Batch 06, Department of CSE, BUET</span></p>
                    <p><span style="font-size: medium;">3. Md. Badsha Mollah, Batch 06, Department of CSE, BUET</span></p>
                </div>
                <div id="toll-collection" class="tab-pane">
                    <h1><span style="font-size: large;">AUTOMATED TOLL COLLECTION SYATEM</span></h1>
                    <img class="img-polaroid" src="assets/images/hardware/image007.jpg"/>
                    <h1><strong><span style="font-size: medium;">Project Description:</span> </strong></h1>
                    <p><span style="font-size: medium;">In this system we will identify each vehicle uniquely with a RFID-TAG. We will attach a RFID-TAG as a sticker with every vehicle during the registration process. That RFID-TAG will bear the unique identification number for that particular vehicle. During the registration process the vehicle&rsquo;s owner will be asked to provide the following information registration no, owner&rsquo;s national id, phone number, bank account no and the name of the bank. In each automated toll booth we will have a RFID sensor and a load sensor set up along with the control booth which will house a computer and an operator. Whenever a vehicle will pass through the booth the RFID sensor will read its identification number from the sticker it&rsquo;s carrying. And the load sensor will measure the vehicle&rsquo;s weight and the amount of toll to be paid will be shown in an LCD display. The toll money will be deduced from the vehicle owner&rsquo;s bank account and he will be notified of the transaction via a SMS. If the owner&rsquo;s bank account is out of money then this payment will be marked as pending and he will be asked to pay it as soon as possible via a SMS. If a pending payment is not cleared within 15 days then that vehicle will be blacklisted. If such a vehicle comes to the booth that doesn&rsquo;t have a RFID-TAG or hasn&rsquo;t been registered yet or the RFID-TAG has been destroyed somehow, then the gate will be automatically closed and the vehicle will be shown the direction to go to the counter following a different lane. In the counter the vehicle can register providing all the information needed and can have its own RFID-TAG attached or it can just pay the cash and pass by.</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><br clear="ALL" /><span style="font-size: medium;"> <strong>Featuring Image :</strong></span></p>
                    <img class="img-polaroid" src="assets/images/hardware/image006.jpg"/>
                    <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><strong>Project Participant: </strong></span></p>
                    <ol>
                        <li><span style="font-size: medium;"><strong></strong><strong>Shihab&nbsp; 07&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span></li>
                        <li><span style="font-size: medium;"><strong></strong><strong>Naushad 07&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span></li>
                        <li><span style="font-size: medium;"><strong></strong><strong>Mushfiq&nbsp; 07 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></span></li>
                        <li><span style="font-size: medium;"><strong>Ayon  07</strong></span></li>
                        <li><span style="font-size: medium;"><strong>Mehedi 07</strong></span></li>
                        <li><span style="font-size: medium;"><strong>Avijoy 07</strong></span></li>
                    </ol>
                    <p><strong>&nbsp;</strong></p>
                    <h1><strong><span style="text-decoration: underline;"><span style="font-size: large;">Acheivement Of This Project :</span><br /> </span></strong></h1>
                    <ul>
                        <li style="font-size: medium;"><strong>This project acquires &ldquo;Champion&rdquo; place in Inter-University Project Show in CSE Fest-2011 held in Buet .</strong></li>
                    </ul>
                </div>
                <div id="token-both" class="tab-pane">
                    <h1><span style="font-size: large;">Smart Token Booth / Queue Management System</span></h1>
                    <div class="row-fluid">
                        <div class="span6">
                            <img class="img-polaroid" src="assets/images/hardware/image008.jpg"/>
                        </div>
                    </div>
                    <h1 style="font-size: medium;">Team Members</h1>
                    <ol>
                        <li>
                            <p><span style="font-size: medium;"><strong> ShafiulAzamChowdhury Batch 07 <br /></strong></span></p>
                        </li>
                        <li>
                            <p><span style="font-size: medium;"><strong>Md Ali Nayeem Batch 07</strong></span></p>
                        </li>
                        <li>
                            <p><span style="font-size: medium;"><strong>Dipranjan Das Batch 07</strong></span></p>
                        </li>
                        <li>
                            <p><span style="font-size: medium;"><strong>MdEnzamHossain Batch 07 </strong></span></p>
                        </li>
                        <li>
                            <p><span style="font-size: medium;"><strong>Md Imran HasanHira Batch 07</strong></span></p>
                        </li>
                        <li>
                            <p><span style="font-size: medium;"><strong>Abu HasanMasud batch 07</strong></span></p>

                            <p><span style="font-size: medium;"><strong>&nbsp;</strong></span></p>
                        </li>
                    </ol>
                    <h1><strong><span style="font-size: medium;">Project Description:</span> </strong></h1>
                    <p><span style="font-size: medium;">Our project was about developing a system to ease traditional customer queue management, found at Banks, doctors' chambers or various customer care centers. In these, people need to stand in a queue, waiting for being served. We specially focused on developing&nbsp;an&nbsp;smart system specially for banks. Features were:</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Many service booths&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Differentiated services</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Independent modules&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wireless communication between modules</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Central server&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Report Generation</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Token Printing</span></p>
                    <p><span style="font-size: medium;">Among these, Wireless communication between modules could not be implemented due to device problems, and token printing did not seem feasible due to unavailability of printers at an affordable price.</span></p>
                    <p><span style="font-size: medium;"><strong>Project Feature:</strong></span></p>
                    <ol>
                        <li><span style="font-size: medium;">Different &nbsp;independent modules have been created such as &nbsp;two modules for two differentiated service (Money Withdrawal &amp; Customer Help Query), one module for taking token, and one module for controlling all these modules, connected with a computer with software support.</span></li>
                        <li><span style="font-size: medium;">There are also other services such as Service Booth Modules ,Token Booth Module , Controlling ModuleCommunication Protocol .</span></li>
                    </ol>
                </div>
                <div id="water-facet" class="tab-pane">
                    <h1><span style="font-size: large;">Automated Water Faucet</span></h1>
                    <div class="row-fluid">
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/hardware/image009.jpg"/>
                        </div>
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/hardware/image010.jpg"/>
                        </div>
                    </div>
                    <h1><strong><span style="font-size: medium;">Project Description:</span> </strong></h1>
                    <p><span style="font-size: medium;">The main focus of our project was to bring something innovative that will reduce the misuse of water into our existing water faucets. In this regard, we pointed out a fact that whenever there is no water in tank but someone has kept the tape opened carelessly, then when there is water available, it is simply wasted. Because most of the time, we do not remember that the tape is open. So, we implemented an automated water faucet system that will remove manual movement of faucet and thus supply water only whenever it detects a user.</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;"><strong>Project Feature:</strong></span></p>
                    <p><span style="font-size: medium;">&nbsp; Small portable device with DC energy source</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;Can be added to the existing Water Faucets as an external device</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;Can be used for variable sizes of faucets</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;Cost Effective</span></p>
                    <p><span style="font-size: medium;">&nbsp;</span></p>
                    <p><span style="font-size: medium;">&nbsp;&nbsp;Can absorb the pressure of 10 storied water tank<strong> <br /><br /></strong></span></p>
                    <p><span style="font-size: medium;"><strong>Project Members :<br /> </strong>1.0705067 - KaziAbir Adnan</span></p>
                    <p><span style="font-size: medium;">2.&nbsp;&nbsp;&nbsp;&nbsp;0705074 - FahimTahmidChowdhury</span></p>
                    <p><span style="font-size: medium;">3.&nbsp;&nbsp;&nbsp;&nbsp;0705080 - Syed EsrarMahbub</span></p>
                    <p><span style="font-size: medium;">4.&nbsp;&nbsp;&nbsp;&nbsp;0705084 - Md. MonsurHossain</span></p>
                    <p><span style="font-size: medium;">5.&nbsp;&nbsp;&nbsp;&nbsp;0705085 - Md. Mahbub-Ul-Haque</span></p>
                    <p><span style="font-size: medium;">6.&nbsp;&nbsp;&nbsp;&nbsp;0705088 - Md. Mohsin</span></p>
                </div>
                <div id="sms-home" class="tab-pane" >
                    <h1><span style="font-size: large;">SMS BASED HOME APPLIANCE</span></h1>
                    <div class="row-fluid">
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/hardware/image011.jpg"/>
                        </div>
                        <div class="span4">
                            <img class="img-polaroid" src="assets/images/hardware/image012.jpg"/>
                        </div>
                    </div>
                    <p><span style="font-size: medium;"><strong>Project description/Feature:</strong></span></p>
                    <div style="font-size: medium;">
                        <p>By Messaging(SMS) to a specific cell number,switching fan/light on/off.Sometimes we forget to switch fan/light off and leave home/office,we can then switch fan/light off.This project also can be used to switch air conditioner on before reaching home to make the room cool.</p>
                        <p>Project Feature :</p>
                        <p>1 .This project can be used to switch air codition on before reaching home.<br /> 2. It also can be used with fire alarm.<br /> 3. By SMS we can switch off fan/light on/off. <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4. Also by SMS we can switch off/on air-conditioner</p>
                        <p>Featuring Image :</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>Project Members: <br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Md. MarufHossain(0705007)</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. ShuvasishKarmakerShuvo(0705008)</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. DipankarRanjanBaisya (0705018)</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4. Mir Md. Faysal0705026)</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5. Md. MazharulIslam(0705041)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>