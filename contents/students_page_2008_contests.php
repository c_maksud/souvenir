<div class="row-fluid">
    <table class="table">
        <thead></thead>
        <tbody>
            <tr id="samsung-contest">
        <div>
            <h1><span style="font-size: large;">Samsung Android App Contest (Gold Status Winner):</span></h1>
            <div class="row-fluid">
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image040.jpg"/>
                </div>
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image042.jpg"/>
                </div>
                <div class="span4">
                    <img class="img-polaroid" src="assets/images/students/image044.jpg"/>
                </div>
            </div>
        </div>

        <h1><span style="font-size: medium;"><strong>Project Name: </strong>Tell Tale &ndash; Approach to communal writing</span></h1>
        <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Azad Abdus Salam(0805030)</span></li>
            <li><span style="font-size: medium;">KamrulHasan(0805018)</span></li>
            <li><span style="font-size: medium;">Tanveer Mohammad Khan(0805027)<br /></span><span style="font-size: medium;"><strong><span style="text-decoration: underline;"> <br /></span></strong></span></li>
        </ul>
        <p><span style="font-size: medium;">Tell Tale &ndash; Approach to communal writing&rdquo; innovates a way to write story/poem/rhyme communally. Each story is assembled by parts, where each part is contributed by different person from the community. This app can have great impact on society by increasing the practice of creative thinking, imagination and open ended writing among people especially the young generation.&nbsp; The Android version of &ldquo;Tell Tale&rdquo; participated in Android Application Development Contest by Samsung Bangladesh R&amp;D Center and received the &ldquo;Gold Status&rdquo; (1st prize) .</span></p>
        </tr>
        <tr id="hephe">
        <div>
            <h1><span style="font-size: large;">Project HEPHE(4<sup>th</sup> in Imagine Cup, Bangladesh, 2011)</span></h1>
            <img class="img-polaroid" src="assets/images/students/image001.jpg"/>
        </div>
        <p align="center">&nbsp;</p>
        <h1 style="font-size: medium;">Team Members:</h1>
        <ul>
            <li><span style="font-size: medium;">Md. Abdul MunimDibosh (0805033)</span></li>
            <li><span style="font-size: medium;">Md. MaksudAlamChowdhury (0805034)</span></li>
            <li><span style="font-size: medium;">Md. NazmulHossain (0805032)</span></li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">The word HEPHE stands for Hunger, Education, Poverty, Health &amp; Environment; a project that was targeted to solve all these severe problems existing in 3<sup>rd</sup> world countries. The primary initiative of the software is to provide primary education in the mean of games and easy to adopt techniques. This creates a bridge in between educated and non-educated people and helps to develop leadership among them. The idea and the developed prototype were appreciated a lot by the then judges of Imagine Cup country round. The software was built using .NET framework, specifically Winforms&amp; Rad Controls for application front end.</span></p>
        </tr>
        <tr id="e-learning-for-autistic">
        <div>
            <h1><span style="font-size: large;">East West University IT Fair 2011(1<sup>st</sup> Prize in Project Section):</span></h1>
            <img class="img-polaroid" src="assets/images/students/image016.jpg"/>
        </div>
        <h1><span style="font-size: large;"><strong>Project name:</strong> "E-Learning System and Software for Autistic Children"</span></h1>
        <h1 style="font-size: medium;">Team Members:</h1>
        <ul>
            <li style="font-size: medium;">Anika Anwar (0805083),</li>
            <li style="font-size: medium;">Ishrat Ahmed Maureen (0805064),</li>
            <li style="font-size: medium;">SamihaSamrose (0805066),</li>
            <li style="font-size: medium;">Anan Naha,</li>
            <li style="font-size: medium;">Syed Ishtiaque Ahmed.</li>
        </ul>
        <p align="center">&nbsp;</p>
        <p  style="font-size: medium;">On 5th February,2011, Anika Anwar,Ishrat Ahmed and SamihaSamrose participated in the inter university software project show arranged by East West University. &ldquo;E-Learning System and Software for Autistic Children&rdquo; was developed for autistic children to help them learn computers and become more interactive socially and mentally. Syed Ishtiaque Ahmed, mentor of the team and Anan Naha from &rsquo;05 batch helped to a great extent. The project won 1<sup>st</sup> prize because of its innovativeness.</p>
        </tr>
        <tr id="anvillab">
        <div>
            <h1><span style="font-size: large;">ANVIL LAB Business Manager</span></h1>
            <img class="img-polaroid" src="assets/images/students/image020.png"/>
        </div>
        <h1><span style="font-size: medium;">Team members:</span></h1>
        <ul>
            <li><span style="font-size: medium;">Md. Hasan Al Maruf (0805011)</span></li>
            <li><span style="font-size: medium;">Sharif Md. SaadGalib (0805014)</span></li>
            <li><span style="font-size: medium;">Md. Abdul MunimDibosh (0805033)</span></li>
            <li><span style="font-size: medium;">IrtezaNasir (0805054)</span><span style="font-size: medium;"> <br /></span></li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">In the early summer of 2011, four guys from CSE department were planning to build a sales and marketing chain management software as a project for Software Development course. This software was nothing professional when they started to develop it but it became once Ispahani Group of Bangladesh chose it to implement it for their company. Now this software is being used in more than 6 regional offices of Ispahani by 3000+ users. Developed in latest technologies like Microsoft Silverlight and Windows Azure it has been the first this type of software in Bangladesh; which is developed by only 4 undergrad level students.</span></p>
        </tr>
        <tr id="javfest-pinfriend">
        <div>
            <h1><span style="font-size: large;">Therap JAVA Fest (3<sup>rd</sup> Position)</span></h1>
            <div class="row-fluid">
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image014.jpg"/>
                </div>
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image012.jpg"/>
                </div>
            </div>
        </div>
        <h1><span style="font-size: medium;"><strong>Team Name:</strong> BUET BAREBONES</span></h1>
        <p><span style="font-size: medium;"><strong>Team members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Md. MaksudAlamChowdhury (0805034)</span></li>
            <li><span style="font-size: medium;">ATM Saleh (0805025)</span></li>
        </ul>
        <p><span style="font-size: medium;"><strong>Project Name:</strong>PinFriend --Identity &amp; Location based Reminder<br />&nbsp;</span></p>
        <p><span style="font-size: medium;">Therap Java Fest was organized by Therap Services for university students all over the country. Interested participants formed a group of two members and submitted a project proposal and then implemented the idea as a Java web application or an Android-based project. After the Therap Java Fest team evaluated the projects, top ranked participants were invited for a live demonstration of their projects on November 14, 2012. BUET BAREBONESbecame 3rd in the contest and won two Kindles. They developed an Android app named &ldquo;PinFriend --Identity &amp; Location based Reminder&rdquo;</span></p>
        </tr>
        </tbody>
        <tr id="prothom-alo-youth-fest">
        <div>
            <h1><span style="font-size: large;">TIB-ProthomAlo Youth Festival, Inter University Project Show 2011Champion in Software Category (CUET):</span></h1>
            <div class="row-fluid">
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image022.jpg"/>
                </div>
                <div class="span6">
                    <img class="img-polaroid" src="assets/images/students/image024.jpg"/>
                </div>
            </div>
        </div>
        <h1><span style="font-size: medium;"><strong>Project Name:</strong>Location Based Information System Using OpenStreetMap</span></h1>
        <p><span style="font-size: medium;"><strong>Group Name:</strong> Skyward</span></p>
        <p><span style="font-size: medium;"><strong>Team Members:</strong></span></p>
        <ul>
            <li><span style="font-size: medium;">Md.RashidujjamanRifat (0805107)</span></li>
            <li><span style="font-size: medium;">ShubramiMoutushy (0805082)</span></li>
        </ul>
        <p><span style="font-size: medium;">&nbsp;</span></p>
        <p><span style="font-size: medium;">12-14 May,2011 &ldquo;TIB ProthomAloTarunnoUtsob&rdquo; was held at Chittagong University of Engineering and Technology (CUET). About 16 University participated on that event. The event had Project show, Inter University Debate Competition, Cartoon Exhibition against Corruption and Short film Exhibition.Project show was divided into two categories &ndash; software and Hardware. About 22 projects from 8 different Universities competed there.The judge panel was conducted by TousifSayed (Head of Computer Science and Engineering, Primier University), Professor Dr. DelwarHossain, A.H.M SahanulAlam (Chief Engineer of Intel Soft Solution). In software Category, &ldquo;Location Based Information System Using OpenStreetMap&rdquo; was declared champion.</span></p>
        </tr>
        <tr id="app-a-thon">
        <div>
            <h1><span style="font-size: large;">Microsoft App-a-thon 2012:Prize in &lsquo;Top Windows Phone App&rsquo;</span></h1>
            <img class="img-polaroid" src="assets/images/students/image026.jpg"/>
        </div>
        <h1><span style="font-size: medium;"><strong>Name: &nbsp;</strong>HafsaZannat (0805113)</span></h1>
        <p><span style="font-size: medium;">Microsoft introduces the very first app development marathon, &ldquo;<strong>Microsoft App-a-thon</strong>&rdquo;, in Bangladesh on the Windows 8 and Windows Phone platform. This daylong event took place on the November 3, 2012 in the Institute of Information Technology (IIT) of Dhaka University. Ultimately, the participants come up with 100 apps surpassing the initial target of submitting 80 apps to windows store. Eight of those got the <strong>&lsquo;Top App&rsquo;</strong> award. Students from BUET, DU, AIUB, NSU, KUET, EWU, BRAC University and UIU participated in this event. Besides, as a part of the countrywide preparation, Microsoft Student Partners hosted workshops on those 8 universities.</span></p>
        </tr>
    </table>
</div>