<?php include_once 'templates/header.php'; ?>
<?php include_once 'templates/menubar.php'; ?>
<?php

if (isset($_GET['page'])) {

    include_once "contents/" . $_GET['page'] . '_page.php';
} else {
    include_once "contents/" . 'students' . '_page.php';
}
?>
<?php include_once 'templates/footer.php'; ?>