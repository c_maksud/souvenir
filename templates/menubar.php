<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <a class="brand" href="index.php">BUET CSE Achievements</a>
        <ul class="nav">
            <li><a href="index.php">Students' Achievements</a></li>
            <li><a href="index.php?page=faculty"> Faculty Achievements</a></li>
            <li><a href="index.php?page=alumni">Alumni Achievements</a></li>
            <li><a href="index.php?page=programming_contest">Programming Contest Achievements</a></li>
            <li><a href="index.php?page=hardware_projects">Hardware Projects</a></li>
            <li><a href="index.php?page=software_projects">Software Projects</a></li>
            <li><a href="index.php?page=labs">Lab Facility</a></li>

        </ul>
    </div>
</div>
<br/>
<br/>
<br/>
<br/>